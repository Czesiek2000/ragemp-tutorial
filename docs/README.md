---
home: true
heroImage: /hero.png
heroText: RAGEMP TUTORIAL
tagline: Javascript tutorial for RAGEMP newbies 
actionText: Get Started →
actionLink: /guide/
features:
- title: What's here
  details: This is simple tutorial that introduce new users to RAGEMP development world
- title: Why RAGEMP
  details: Javascript support, well documented API, big forum willing to help and contains lot of useful informations
- title: What RAGEMP offers
  details: Play with your friends, host a server, create a community or create an entirely new gamemode.
footer: MIT Licensed | Copyright © 2020-present Czesiek2000
---
