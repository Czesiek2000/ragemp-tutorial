# CEF introduction
We won't cover here whole HTML and CSS that are main part of UI (User Interfaces) displayed in game with CEF(Chromium Embeded Framework, this is name of implementation chrome browser engine in applications like RAGEMP. You can read more about CEF [here](https://en.wikipedia.org/wiki/Chromium_Embedded_Framework)). 

Worth mentioned is that when you open `index.html` in browser and inside game you will notice some difference. That's because RAGEMP do some magic and delete white background that is in your browser. Of course you can set background for your UI.

You can read about 
* HTML [here](https://www.w3schools.com/html/) or [here](https://www.tutorialspoint.com/html/index.htm)
* CSS [here](https://www.w3schools.com/css/) or [here](https://www.tutorialspoint.com/css/index.htm)

## File structure
```bash
..
├── client_packages             # Client side code
│   ├── cef                     # Our resource 
│       ├── ui                  # UI folder
│           ├── assets          # OPTIONAL folder for images or something external
│           └── index.html      
│           └── script.js
│           └── styles.css
│       └── index.js            # Main index.js file

```

## Files
### HTML

Here you have basic template for HTML file.

```html
<!DOCTYPE HTML>
<html>
<head>
<title>Here title of your website</title> <!-- That does not appear in game it only appears in browser when developing -->
<style>
    /* Internal styles here or attach external file instead of style tag */
</style>

<!-- Place path to script, that handles clicks or other ui interactions, inside "" of src -->
<script src=""></script>
</body>
</html>
```
Here we will create basic CEF interface. You can also view this in your browser.

```html
<!-- index.html -->
<!DOCTYPE HTML>
<html>
<head>
<title>RAGEMP CEF tutorial</title>
<!-- Here link css file -->
<link rel="stylesheet" href="style.css">
</head>
<body>
<div id="main" class="main">
    
    <button id="btn" class="btn">Click for some action</div>
    <div id="output">
        <!-- Here we will render content after click -->
    </div>
</div>
<!-- Here link javascript file -->
<script src="script.js"></script>
</body>
```

::: tip 
We can write css inside html between `<style>` and `</style>` and put it between `<head>` and `</head>` tags. 
:::
Look at code sample for this section [here](https://gitlab.com/Czesiek2000/ragemp-tutorial/-/tree/master/code_samples/client_packages/cef/ui/index.html)

### CSS
Css file is used for styling. It is optional because if we have to add small styling we can do it inside html between `<style>` and `</style>` tags, like it is shown in html file.

We want look at it in this tutorial. You can view source of [here](https://gitlab.com/Czesiek2000/ragemp-tutorial/-/tree/master/code_samples/client_packages/cef/ui/style.css)

### Javascript ui
Javascript can be used in UI the same as in browser. In this tutorial we separate ui file with client file by naming them: 
* Client ui javascript as `script.js`
* Server client as `index.js`
::: tip
Also note that server client side files and ui files are placed in different directories.
:::

Know lets look at some code.
```js
const btn = document.getElementById('btn');
const btn2 = document.getElementById('btn2');
const output = document.getElementById('output');
const field = document.querySelector('.field');
```
Here we are getting all values from website and assign them to variables for future use inside code.

Do some action on this page will be much better than this boring site ? So lets look how we can do it.
```js
btn.addEventListener('click', () => { // Add event listener for button
    // This will display text in browser
    output.innerHTML = "Text display after click button in browser" 
});
```
Add this code to you source code and look what happend inside your game. 
In javascript inside browser we can add something called event listeners to listen for some actions like click in our case. Event listener needs callback function, inside it we use `innerHTML` option on `div` called `output` to add text that will display on screen.

```js
btn2.addEventListener('click', () => {
    // Get value of field
    const fieldValue = field.value; 
    // Check if value is not empty
    if (fieldValue === "") {
        // If empty output message to screen
        output.innerHTML = "Input value cannot be empty"
    }else {
        // If not send data to client
        console.log(fieldValue);
        // This will send data to from CEF to client
        mp.trigger('fromBrowser', fieldValue);
    }
})
```

Here we're adding another listener to other button. Then we assing to variable called `fieldValue` value that user enter to input text field.

To prevent this example to be simple, boring and only adding listener and displaying text after click we add some simple validation to check if user enter something to input and display proper text based on field state :fire:.

If user enter nothing and click submit button, text Input value cannot be empty will display on screen otherwise we will send data to client and show notification above map with value that was entered to text field.

**NOTE: `console.log()` in this example will only log something in browser**

### Assets
When you want to use some assets like images etc. You should placed it inside folder where is `html` file, in our case inside `ui` folder(this is done to simplify finding path to our assets, but you can place assets in diffrent location). For cleaner file structure you can use folder for better organization of image files.

### Index.js
This file is used to handle browser creation and destruction inside server client side. Also you can write there other client side scripts. 

:::tip
To keep clean file structure and avoid chaos inside your files and folders you should separate code to files into files and folders that the code aplies to.
:::

```js
let browser;

// Add command to chat to open CEF
mp.events.add("playerCommand", (command) => {
    const args = command.split(/[ ]+/)
    const commandName = args[0]
    args.shift();
    
    // Value inside "" is our chat command 
    if (commandName === "example") {
    
        // Create browser 
        browser = mp.browsers.new("package://cef/ui/index.html");
    
        // Display coursor
        setTimeout(() => {
            mp.gui.cursor.show(true, true);
        }, 500);
    
        // Show notification (optional)
        mp.game.graphics.notify("Example notification")

    }
```

We have basic knowledge on building basic UI. So now let's open it inside our server / game. We can do this with above code. But let's find out what is happening here.

```js
let browser;
```
In this line we declare variable called browser to get access inside all our code later on.

Next we add built-in client side event called `playerCommand` which is used for client side chat command handling. This is also clientside implementation of serverside `mp.events.addCommand`. 

Callback function parameter is command that we enter to chat for example:
```text
/exampleCmd This is example
```

Inside `command` variable will be stored whole string (exampleCmd This is example)

```js
const args = command.split(/[ ]+/)
```
**Command** variable is string, on string we can use method called **split**. Split method is used for create array from given string. 

If we didn't use regular expression (short regex) as split parameter we wouldn't get array with each word as separate value. Hovever we will get array with one value which will be our whole command string, which is not what we want to accomplish.

```js
const commandName = args[0]
```

That value is self examplained. Basicly we get first value from **args** array and assign it to **commandName**. Which we will use later to check if value that was entered to chat after **/** is our command that we want to handle. 

```js
args.shift()
```
That method remove first value from **args** array.

Then inside if / else block we check if first value in array that we assing to **commandName** variable is the same as string inside quotes("")

```js
browser = mp.browsers.new("package://cef/ui/index.html");
```
After that we create instance of our game browser and assing it to browser variable. In ragemp to link html file which we want to display we need to start with **package** which is know as package protocol, relative path to our client side folder. So if we have path like `path/to/RAGMEP/client_packages/cef/ui/index.html` we need to specify path to `index.html` file like in code (You can use this in your code with change of **cef** to folder where you keep your resource).

More on on wiki [here](https://wiki.rage.mp/index.php?title=Package_Protocol)

Next we want to show cursor inside game. To do that we need to use simple method that has example on [wiki](https://wiki.rage.mp/index.php?title=Cursor.show) which we will use for this example.
```js
mp.gui.cursor.show(freezeControls, visibility);
mp.gui.cursor.show(true, true);
```

Then if we want to hide cursor we should use the same method but change parameters, which should look like this.
```js
mp.gui.cursor.show(false, false);
```
:::tip
In RAGEMP 1.0+ when we want to show cursor and in the same block of code something else, we need to use `setTimeout` to show cursor proper way, otherwise something is not working correctly and cursor is not visible.

```js
setTimeout(() => {
    mp.gui.cursor.show(true, true);
}, 500);
```
:::

#### Preview
After you put everything correctly in your game folder you should then have access to command example. So then enter to your server press **T** on your keyboard to open chat window and then type `/example`, to run clientside command that we have written. 

Then in top left corner should appear something similar to: 

<ImageLinks src="cef_preview.png" alt="cef_preview" />

After click first button you should see some text that appear below form.

<ImageLinks src="cef_preview2.png" alt="cef_preview2" />

You can play with this code, change something and see what's happend.

<Edit path="cef.md"/>