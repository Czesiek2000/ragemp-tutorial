# Client modules
On client side we also can split code into separate files. We will use peds, weapons, vehicles data from previous tutorial [section](node.md#modules). For this tutorial purpose we will add data from data folder and our gamemode folder will be named gamemode.

## Example
```js
// skins.js
exports = [
    'mp_m_freeemode_01',
    'player_zero',
    'player_one',
    'player_two',
    // And lots more can be here
]

// vehicle.js
exports = [
    'adder',
    'turismor',
    't20',
    'cheburek',
    // And lots more can be here
]

// weapon.js
exports = [
    'weapon_grenade',
    'weapon_grenadelauncher',
    'weapon_carbinerifle',
    // And lots more can be here
]

// Client side index.js
require('./gamemode/data/skins'); // Path on clientside
require('./gamemode/data/vehicles');
require('./gamemode/data/weapons');
```

If you compare this example to example from serverside you will notice that there are some difference between require modules on both sides. Client side modules require has diffrent syntax. To require file in root file we need to add path to file not as server side with `./` but we need to add path as we will look from `index.js` client side root file.

<Edit path="client_exports.md" />