# Introduction
<!-- ### Badge <Badge text="beta" type="warning"/> <Badge text="default theme"/> -->
RAGE Multiplayer (short **RAGEMP**) is alternative, enjoyable and stable multiplayer for Grand Theft Auto V (GTA V).

Project started in May 2016 and is developed all the time until now and doesn't stop.

RAGE is a complete, stand-alone client with great functionality. The team focuses on continuous performance and stability improvement while simultaneously adding new features and expanding the API.

## GTA:Network cooperative
In July 2017, RAGE Multiplayer team announce cooperative with **GTA:Network** (another alternative **GTAV** server which on mainly you can use **C#** to create resources). Until now team doesn't announce name of the project and staying with no change as RAGE Multiplayer.


## Scripting
RAGEMP provides powerful API for developers to build custom resources or full gamemode servers.

It is using <strong>Nodejs </strong> on Server-side and **Javscript** on Client-side. Also for rendering custom UI (made with HTML/CSS/JS and WebGL) use **CEF** (Chromium Embeded Framework). 
Thanks to cooperative with GTA:Network RAGEMP has developed brigde to import GTA:Network scripts and also usage **C#** as scripting language instead of (Nodejs / Javascript).

## Requirements
To start with RAGEMP scripting you need to have:
1. Using Windows operating system (prefered **Windows 10**). This tutorial is made for **Windows** system.
2. Text code text editor (ex. Visual Studio Code - recomended other like sublime or atom will also work).
3. RAGEMP client and server (will get to this soon).
4. Basic knowledge of HTML/CSS/JS (don't worry some stuff will be shown in this tutorial later on).
5. Know how to use terminal with basic commands.

If all above are true for you, this means that you are prepared to start learning and making your own RAGMEP scripts or gamemodes.

<Edit path="README.md" />