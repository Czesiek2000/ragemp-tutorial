# Events
In this section we will learn more about events. We will cover here server side events.

Now we will handle event that we can call inside `commands.js` file. 

```js
// commands.js
mp.events.addCommand("setweather", (player, _, weather) => {
    mp.events.call("setWeather" weather);
})

// events.js
mp.events.add("setWeather", (player, weather) => {
    mp.world.weather = weather;
    player.outputChatBox(`Weather changed from ${mp.world.weather} to ${weather}`)
})
```

In above example we registered command to set weather with given parameter. Then we call server-side event in another file and send parameter to external event. Then we add event that handle event which we send and inside that event we get sended parameter. Then we change value of `mp.world.weather` which set value of game weather to parameter that we received from command which we send from **setweather** serverside command.

We can call event from another or in the same file inside serverside. Inside events we can pass parameter, or receive parameters that we send with `mp.events.call` function.

## Game events
Now we will handle basic events which will handle few serverside actions like whe player join game, quit or dies which we mentioned in previous section.

To handle mentioned event we will use events syntax.
```js
mp.events.add("<eventName>", (player, parameter) => {
    // some code here
})
```

Inside `events.js` file we store position coords inside position object. This coords are mazebank coords.
```js
let spawnPos = {
    X: -71.9208,
    Y: -817.9440,
    Z: 326.0485
}
```
### Join event
This event will handle action that server should do when player join or log to server. We can do with syntax like

```js
mp.events.add('playerJoin', (player) => {
    player.model = 'mp_m_freemode_01';
    player.spawn(new mp.Vector3(spawnPos.X, spawnPos.Y, spawnPos.Z))
    player.notify(`~b~ ${player.name} ~w~spawned`)
})
```

In second line we set model with `player.model` syntax we set model to multiplayer base model. 

In third line we spawn player with `player.spawn` syntax. This function requires coords, which we can set using `Vector3`. We stored values of x, y, z for vector3 arguments and call them with `spawnPos.`

### Notify
Then we call `notify` function that shows notification above map. Which should look like below

<ImageLinks src="notification.png" alt="notification" />

RAGEMP also supports displaying color notification text.

On server side you can use 
```js
player.notify('~w~Hello ~b~World');
```
On client side 
```js
mp.game.graphics.notify('~w~Hello ~b~World');
```
Which will create color notification above map which should look like this:

<ImageLinks src="NotificationResult.png" alt="notificationc" />

More options and color values can be found [here](https://wiki.rage.mp/index.php?title=Fonts_and_Colors). Also note that if you use color value in the begining of text whole text will be set to that color value, you need to place another color value before text that you want to have different color.

## Death event
Without this event, when player dies and this event is not implemented inside server and you want to reconnect to your server or player dies you will find infinite loading screen. Implementing this death event inside server where should place player on map.
```js
mp.events.add('playerDeath', (player) => {
    player.spawn(new mp.Vector3(spawnPos.X, spawnPos.Y, spawnPos.Z));
})
```

## Quit event
This event triggers when player quits the game. This example show notification when player quit game.

```js
mp.events.add("playerQuit", (player) => {
    player.notify(`${player.name} leave the server`);
})
```

All events samples can be found inside [gitlab](https://gitlab.com/Czesiek2000/ragemp-tutorial) repo.

<Edit path="event.md" />