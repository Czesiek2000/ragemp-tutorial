# Timing events :clock1:
In Javascript we have functions that can run code after some period of time `setTimeout` or run code in earlier specificied intervals `setIntervals`

## setTimeout syntax
Function `setTimeout` run after time which we pass after callback function (time variable). Method is executed until `clearTimeout` function is called. Worth mentioned is that time passed is in miliseconds, so assume that we want to run some code after 1 second, we will change time variable with 1000. 
```js
// Old syntax 
setTimeout(function () {
    // some code here
}, time) // This is time variable that will be refering to later.

// New syntax
setTimeout(() => {
    // some code here
}, time)
```
How do we know what number add as time variable ? 

Do this quick math: Number in seconds (ex.3) * 1000 = **3000**. 3000 is result, so this is our value in milisecond to assign as time variable.  

### Example of setTimeout
Assume we want to notify player after 2 seconds when successfully logged in. 
```js
mp.events.add('playerJoin', (player) => {
    console.log("player join server");
    let timeout = setTimeout(() => {
        player.notify(`~b~${player.name} ~w~joined ~r~server`); // That will be execute after 2 seconds
    }, 2000);

    clearTimeout(timeout); // Stops setTimeout from infinite running

})
```
# setInterval syntax 
Function setInterval calls rapidly function or executes a code snippet, with a fixed time between each call. 
```js
// Old syntax of setInterval is similar to setTimeout old syntax using function, just change setTimeout to setInterval

setInterval(() => {

}, time) // Here also is time in miliseconds to pass
```

### Example of setInterval
```js
// This example will log to console text inside "" every 2 second, your console will die, don't do it.
let interval = setInterval(() => {
    console.log("Logged every two seconds");
}, 2000)
```

## Combine setTimeout and setInterval
```js
// Declare interval to do function every 5 seconds
let interval = setInterval(() => {
    console.log("This is logged every 5 second").
}, 5000)

let timeout = setTimeout(() => { 
    clearInterval(interval); // This will remove interval function, after 6 s
    console.log("This is timeout test")
}, 6000);

clearTimeout(timeout); // Clear timeout.
```
::: tip
You should clear **interval** and **timeout**, because this functions won't stop and run infinite. 
:::

<Edit path="timing.md" />