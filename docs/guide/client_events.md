# Client side
On client side you have control on the game world, single player, but you have not much informations about players in the game. On client side you can do things like GUI (graphical user interface), visual effect, camera manipulations, create peds and many more.

How to create client_side folders and folder structure was menthioned [here](resource.md#creating-client-side-resource), so we want cover it in this section.

## Client events
Event in client-side has the same syntax as server-side. We can register build in event if browser is created, we will use:  
```js
mp.events.add("browserCreated", (browser) => {
    mp.gui.chat.push("Browser created, yey"); // This will output to chat Browser created, yay
})
```

Client side has different syntax than server-side. 

## Custom client side events
As in server we have also custom events to handle our actions, for example calling from client, or server. Now we will create event that can handle call from another client-side event.

```js
mp.events.add("customEvent", () => {
    mp.gui.chat.push("custom event from client") // Output to chat: custom event from client
});
```

On client side we have event that handle when user enter some command to chat without using server-side event. That is also example of registering event
```js
mp.events.add("playerCommand", (command) => { // Command are all variables that we enter to the chat after /
    const args = command.split(/[ ]+/) // This will create array of all values that we enter to chat
    const commandName = args[0] // Here we assign value of commandName to first value of array or args. Example: /custom hello (custom is commandName)
    args.shift() // Removes second array value
    if (commandName === "customCommand") {
        mp.events.call("customEvent"); // Call event named customEvent 
    }
})
```

You can also check tutorial on RAGEMP Wiki that covers client side [here](https://wiki.rage.mp/index.php?title=Getting_Started_with_Client-side)

<Edit path="client_event.md" />