# Folder structure :file_folder:
In this section we will take closer look at RAGEMP server folder structure.

NOTE: **All server files are stored under folder called server-files.**
## Root files :page_facing_up:
Root folder of RAGEMP looks like this: .

<ImageLinks src="root.png" alt="root" />

For server development very important are following: 
* server-files folder
* ragemp_v.exe
* updater.exe

## Server files :page_facing_up:
Server-files folder looks like this after running `updater.exe` located in root of RAGEMP folder.

<ImageLinks src="newfiles.png" alt="root" />

::: warning
These may look different for you in the beginnning, but after you run `ragemp-server.exe` the same project structure will be generated for you.
:::
What are these folders ?
* client_packages - here you need to place all client side files (created UI and scripts to manage client side functions and events).
* maps - here you will place all maps files (needs to be in JSON format) that will be loaded to the game world.
* packages - here you need to place all server files (scripts to manage server side function and events or player instance).
* plugins - here place all `.dll` plugins for your server. 
* conf.json - RAGEMP configuration file that specifies server configution.
Needs to be in JSON format.
* server.exe / ragemp-server.exe - file that start server.

## Server launching :rocket:
You now know what these files and folder means and doing, so now let's run `ragemp-server.exe`, then console will popup which should look like this: 

<ImageLinks src="console.png" alt="server" />

If you see the same window your server started you can know run `updater.exe` to lauch the game. You can now start building or coding RAGEMP gamemods.
::: tip
Here you will see all errors from server-side :exclamation:.
:::
After successfull lauch `updater.exe` this window should pop up depending on ragemp version (this window can change :exclamation: )

<ImageLinks src="updater_exe.png" alt="updater.exe" />

From this window you can join public servers or connect to your locahost server (what we will do to start developing resources).

<ImageLinks src="server-launch.png" alt="updater_icons" />

This screen show all icons located in the top right corner of the app that opens after launching `updater.exe`. First three from right are icons that everybody know what are doing. Next icon is for setting up some settings for server like your nickname or preferred language. Next icon is for localhost (on your PC) server lauch.

After clicking icon for server launch (first from left in the top left corner), you should see popup like this: 

<ImageLinks src="server_connect.png" alt="connect" />

First field is for server ip, second is for port that server is running and listening. This values can be specified in `conf.json` file which we will look close later [here](./conf.md).

Default values for ip is 127.0.0.1 and for port is 22005.

Correct typed values should look like this: 

<ImageLinks src="address.png" alt="address" />

After correctly filling ip and port fields you can click connect buton, which will do some magic and your server will lauch :tada:. You can now launch the game, enjoy RAGEMP and go to the next step of this tutorial.

## Easier instruction
Here you have easier instruction that I have found in the internet, which explains how to connect to the server

<ImageLinks src="how-to-connect.jpg" alt="how_to_connect" />

This is easy instruction that I have explained you above.

<Edit path="fileStructure.md" />