# Instalation
This page will shortly show you how to install everything that is required to start with RAGEMP developing or gaming.

## Grand Theft Auto and RAGEMP
I assume that if you are interested in creating RAGEMP resources you already have copy of Grand Theft Auto V game, if not visit wiki page thread [here](https://wiki.rage.mp/index.php?title=Buy_Grand_Theft_Auto_V), which will show you where you can get copy of the game. 

NOTE: RAGEMP doesn't support cracked version of GTAV game.

## RAGEMP Prerequirements
For RAGEMP stable instalation it is recomended to have installed latest VC Redist

## RAGEMP Client Instalation 
1. Download [RAGE Multiplayer Web-Installer](http://cdn.rage.mp/client/ragemp-web-installer.exe).

2. Launch installer and follow the instruction by clicking next until you are prompted to launch RAGE Multiplayer.

3. Click finish and it should start the updating process.

<ImageLinks src="updater.png" alt="updater" />

4. After finishing the updater you'll get a permission to launch ragemp_v.exe, accept and wait until the game finally loads.

## RAGEMP Server instalation
If you installed RAGEMP client you will have structure created. Open `server-files/server.exe` (sometimes this file is ragemp-server.exe)

If you want to have local server you can skip above step and download [Download](https://cdn.rage.mp/public/files/RAGEMultiplayer_Setup.exe). Follow instructions that will popup on screen. Then after successfully installed RAGEMP run updater.exe in root of RAGEMP folder where you RAGEMP was installed, after that go to `server-files` and open `server.exe `

If everything mentioned in above instruction is installed correctly you should be able to connect to server (default local host IP: 127.0.0.1:22005 but can be changed in server config under conf.json but we will get to that later)


## Text editor installation
Next you need to install text editor to write some code. Most recommended is Visual Studio Code but every other text editors (ex. Notepad++, Atom) will work fine.

To install Visual Studio Code go to [Link](https://code.visualstudio.com/download) and select windows operating system. 

After that follow instructions on screen to make success instalation of VSCode.

## Dependencies
* [Visual C++ Redistributable for Visual Studio 2015](https://www.microsoft.com/en-us/download/details.aspx?id=48145")

* [.NET Framework 4.6.2](https://www.microsoft.com/en-us/download/details.aspx?id=53344&amp;desc=dotnet462")

## Instalation problems
If you face any problems or issues don't hesitate to ask comunity on [Discord](https://discord.gg/A5exBRX), [Forums](https://rage.mp/forums/forum/5-support/) or [Forum Thread](https://rage.mp/forums/topic/1816-client-troubleshooting-tips-fixes/)

If you have any problems with instalation RAGEMP, you can check [this](https://rage.mp/forums/topic/128-how-to-install-rage-mp/) tutorial by Captien on forum.

<Edit path="start.md" />