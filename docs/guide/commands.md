# Introduction to Commands
RAGE Multiplayer server comes with preinstalled chat resource that allows you to write plain text to chat or execute commands. To catch that user enter command we need to add specific event which on server side is known as below:
```js
mp.events.addCommand('<commandName>', (player) => {
    // Some code here
});

// Or like this
mp.events.addCommand('<commandName>', functionName(player));

function functionName(player) {
    // Some code here
}
```

:::tip
Remember in your resource to rename `functionName` to name of your function that you use.
:::

Above you can see two ways of creating commands for RAGEMP server. Lets take close look what each line and word means. 

## Example
```js
mp.events.addCommand('hello', (player) => {
    player.outputChatBox('world');
})
``` 
In this example we can see basic command. To execute it create folder inside `server-files/packages` do same steps as [here](./resource.md), where we learned how to create resources files and folders. Then enter the game server and hit `t` then type `/hello` and you should see word `world` in chat as response to our command.

## Structure explanation
```js{1}
mp.events.addCommand('<commandName>', (player) => {
    // Your code here ..
});
```
This is how we create commands in RAGEMP. To clear everything, commands are 'handlers' for user chat input, to set diffrence between plain text to display and command to do something inside server like teleport player, spawn vehicle etc.

To handle commands you add specific event for server which is `addCommand`. 
The `<commandName>` is name of your command. After typing `t` chat opens, there you can type text like `hello world` and it will display obove input. 

Back to the `<commandName>` value when type in to chat after `/` server will run handle function `(player) => {}` and code inside pare of curly braces. Function parameter `player` is value that gets informations of player that handle requested function.

In serverside commands **player** should always be **first** argument of callback function. Then after comma you can define your own parameters that you will use in callback function.

## Arguments
If you create command like above, you want to get access to some values that are typed after your command. In ragemp you can define arguments that will store for you that values.

Let's create simple example.
```js
mp.events.addCommand('testCmd', (player, text, ...args) => {
    // Your code here...
})
```

This is basic command declaration with necessary value of player, then text which store all arguments that we enter after `/testCmd` and optional parameters that store single value of **text** variable.

Let's test this is practice with some example like below: 

```js
mp.events.addCommand('testCmd', (player, text, arg, arg1) => {
    console.log(`text: ${text}`);
    console.log(`arg: ${arg}`);
    console.log(`arg1: ${arg1}`);
})
```

If you try typing something like this: 
```text
/testCmd This is test
```

Value of 
* text will be: This is text
* arg - This
* arg1 - is

Checkout your server console and see if above values are the same for you.

Also take a look on example on ragemp [wiki](https://wiki.rage.mp/index.php?title=Getting_Started_with_Commands), where you have differently explained commands.

For more details about addCommand checkout [wiki](https://wiki.rage.mp/index.php?title=Events::addCommand) page.


<Edit path="commands.md" />