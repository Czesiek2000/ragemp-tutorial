# Server events
In RAGE Multiplayer we have something called events. They are registered your requests or support some activity like `playerJoin` or many more. All server-side events you can find [here](https://wiki.rage.mp/index.php?title=Server-side_events)

::: tip 
You can also create client side events, more on that later.
:::

## Types of events
In RAGEMP we have two types of events: 
* Built in like `playerJoin` or `playerDeath`
* Custom which you can create with `mp.events.add("<eventName>")`

## Built in events
These types of events comes with RAGEMP on start and they are auto-triggered after some action happend, like player join server we handle it with built in `playerJoin` event and spawn user at some coords or set user model.

## Custom events
As name said they can be build by gamemode creator (us) and handle stuff that we code inside them.

## Event syntax
**Events syntax are the same on server-side and client-side, so syntax will shown once here.**

Basic syntax of event looks like this:
```js
// This syntax is usually used when using event 
mp.events.add("<eventName>", (player, parameters) => {
    // Some code here
})

// Or 
mp.events.add("<eventName>", handleEventFunction(player, parameters));

function handleEventFunction(player, parameters) {
    // Some code here
}
```

With this syntax we register the event. This registered event goes to the event tree where is doing some magic :tada: to do what's inside this event. 

So look on some example that use this event syntax.

```js
mp.events.add("hello", (player, name) => {
    player.outputChatBox(`Hello ${name}`);
})
```

We registered the event, but we won't see anything in the chat, because somehow we need to call it. So that will be our next topic

## Call event
Calling event means to run code inside that event. For easier use we will use `addCommand` function to register command and then call server event. In server-side we will do it like this
```js
mp.events.addCommand("welcome", (player, _, name) => {
    mp.events.call("hello", player, name);
})
```
We registered the event with player and name variables so that should come when calling this event. We pass player variable that use this command and parameter that user entered to chat.
Usage of this command should look like this: `/welcome Mike`, and output to chat will be: `Hello Mike`

In this example you see how to call event, register event and command. Next we will cover client-side events.

## More information on events
More on events you can check on RAGEMP wiki [here](https://wiki.rage.mp/index.php?title=Getting_Started_with_Events#Introduction).

<Edit path="events.md" />