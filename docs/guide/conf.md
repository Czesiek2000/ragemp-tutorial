## Server configuration :pencil2: 
Below is default config that you will also find [here](https://wiki.rage.mp/index.php?title=Server_settings) with more detailed description of every value. In this tutorial we will shortly cover values that mostly interested for us, server developers.
```js{4,5,6,8,10,11}
{
    {
        "announce": false,
        "bind": "127.0.0.1",
        "gamemode": "freeroam", 
        "name": "RAGE:MP Unofficial server [Tag]", 
        "maxplayers": 100,
        "port": 22005,
        "stream-distance": 500.0,
        "allow-cef-debugging": true,
        "enable-nodejs": true
    }
}

```
Meaning of each highlighted lines:

:four: Here you specified ip address that your server will run (or listen for changes in code)

:five: This is name of gamemode that your server is running, you can change it wil values like roleplay or drift, more [here](https://wiki.rage.mp/index.php?title=Gamemodes). This value will show on the list of available servers after running `updater.exe`

:six: Server name, that will be display in available server list. You can add some tags inside [], for better search result for server users.

:eight: You can change this value to to use another port. 

::: tip
If port is for example 2205, 22006 port will be used for HTTP host for client-packages to download and server files from client.
:::

:one::zero: By setting this to true, you can edit your CEF in the system browser,outside of the game. more on that option [here](https://rage.mp/forums/topic/1306-how-to-enable-cef-debugging/), MrPancakers great tutorial.

:one::one: This value is set by default to true so it is optional to set in inside config file.

<Edit path="conf.md" />