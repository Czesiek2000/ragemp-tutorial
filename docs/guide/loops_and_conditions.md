# More advanced stuff
Javascript is not only about assigning values and `console.log()`. We can do more advance, more programming, stuff such as loops, conditions, and many more.

## Condition
Lets start with conditions because they are really simple and have simple construction. First questions is what are conditions ?. So let me explain that. Conditions are in programming to check some conditions :rofl:. You can check if given values is what you expect or run some code if condition result is positive. To better understand conditions let's look at some code: 

```js{2}
// Conditions body
if(condition) {
    // do something here
}else {
    // do something else
}
```

In highlighted line in () we check if given condition is equals to true or false. If true then do if block of code, but if condition is false do else block.


```js{4}
// Conditions body
if(condition) {
    // do something here
}else if(condition) {
    // do something else
}else {
    // do something else
}

```

Here we check three options. If one of those is true then compiler is doing it's block. This is good for checking more than two conditions. But when checking multiple conditions you code will get little mess and hard to read or maintain.

## Ternary operator
Ternary operator is similar to if but we can write it in one line and has lot of pross and cons in compare to if else block. Ternary operator is shorten version of if

```js
// Sytax 
condition ? val1 : val2
// If condition is true do what ever is after ? else do whatever is after :

// Example
const val = 5;
val == 5 ? console.log(`${val} is 5`) : console.log(`${val} is not 5`) 
```

#### What's going on here ? 

We assign value of 5 to variable named `val`. Then we write ternary operator to check if val is equals to 5. If val is 5 then we log `${val} is 5` if not we log `${val} is not 5`.

## == or === ?
When we are looking on conditions it is worth mention that we have two ways we can check condition.
Diffrence between `== and ===` `==` operator compare only values and `===` compare values and types of two variable. It is better to use `===` operator because sometimes it generates unexpected errors.

Let's see simple example how these operators are working.
```js
const a = 0;
const b = '0';

console.log(a == b); // true
console.log(a === b); // false
```

## Switch
Switch is similar to if, check some conditions, but it makes your code more readable and maintain.
```js
// Syntax
switch(value) {
    case yourCase:
        // Do something here
        break;
    ...
    default:
        // Do something here when value doesn't match to any case
        break;
}
```

In switch we can have multiple cases (which is represented by `...`), but at the end of each case we need to put break keyword. Break means that compiler do everything in case and stops checking other cases when see break. Without break keyword compiler will go to the next cases and do other stuff, which is not what we want to happen.

Default keyword show stuff when value in `()` doesn't not much above cases. This is good to display some warning to user that enter wrong value which your program doesn't support.

Let's now look at example to better understand and remember switch.
```js
const x = 1;
switch (x) {
    case 0:
        console.log('x is 0'); 
        break;

    case 1: 
        console.log("x is 1"); // x is 1 so this will be logged
    case 2: 
        console.log("x is 2"); // This also will be logged because we didn't put break above
        break;

    default:
        console.log("x is not 0, 1 or 2");
        break;
}
```

# Loops

## For Loop
This is basic for loop that is similar to for loop in every other programming languages like Java, C++, Python and so on.
This loop will execute code from value that is declared until condition is equals to true. If condition is equals to false it will stop. 
End value can be end of array length or number etc.
```js
//   declaration   condition      incrementation
for (let i = 0;   i < endValue;     i++) {
    // Do something here 
}

// We can skip statement 1 and declare values outside of for loop
var i = 2;
var len = 10;

for (; i < len; i+=2) {
    console.log(i); // 2, 4, 6, 8
}
```

### Example
```js
for (let i = 0; i < 10; i++) {
    console.log(`i ${i}`);
}
```

## Infinite for loop
We can create infinite loop with for loop. It will break every program without good condition that will end this loop

```js
for (;;) {
    console.log(`This is infinite loop`) // This will infite log inside a loop
}
```

## forEach loop
This function executes once a function for each element in array, in order.

NOTE: This function is not execute on empty array

HINT: This is the easiest way to display values from array
```js
array.forEach((value) => { // Array - array that we iterate over, value single element from array

    // Do something here
})
```
```js
let arr = [1,2,3];
arr.forEach((a) => {
    console.log(a); // 1 2 3
})
```

## for..in loop
This loop loops through object properties.
NOTE: This is good for displaying elements and their values from object.
```js
for (variable in iterated value) {
    // Do something here
}
```
### Example
```js
let person = {
    name: "John", 
    surname: "Doe",
    age: 25
}

for (p in person) {
    console.log(`${p}: ${person[p]}`)
    /* 
    * Ouput: 
        name: John,
        surname: Doe,
        age: 25
    */
}
```

## for..of loop
This loop is useful for iterating over iterated objects. It let's you loop over Array, Object, String, Map, NodeList
```js
for(varible of iteratedValue) {
    // do something here
}

```
### Example
```js
let testString = "test String"
for(string of testString) {
    console.log(string) // display every letter of testString in new line
}

```

## Map
Creates new array contains result of map return value, defined for each element of array.
```js
array.map((value) => {
    // do something here
})
```

### Example
```js
// Combine forEach loop with map method
let numbers = [1,2,3,4];
let times2 = numbers.map((num) => {
    return num * 2; // [2, 4, 6, 8]
})

times2.forEach((times) => {
    console.log(times); // 2 4 6 8
})
```

## while loop
While loop is used to call block of code until given condition is return to true 
```js
while (condition) {
    // do something here
}
```

### Example
```js
x = 0
while (x !== 5) {
    console.log(x); // display numbers from 0 to 4 in new line
    x++; // Increment value of x
}
```
If there want be line 4 in this code, this will be infinite loop, because condition `0 !== 5` will always be true. This cannot be diffrent because value of x won't change and always be 0
## do while
Similar to while loop, diffrence is that first it checks value of condition then doing loop until given condition is equals to true.
```js
do {
    // Do something here.
}while ( condition )
```

### Example
```js
let i = 0;
do {
    i++;
    console.log(i); // This will display values from 1 to 5 in each line.
}while (i < 5);

```

<Edit path="loops_and_conditions.md" />