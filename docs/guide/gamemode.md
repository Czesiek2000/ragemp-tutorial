# Creating resource
After we learn something and know much more how to work with RAGEMP we can use this knowledge in some practical example. We will create basic small gamemode where we will use stuff that we learn earlier and learn some new stuff in practice. So let's get started, shall we ?

## Creating files and folders
When starting new project on ragemp we need to setup our project. First we will setup server side and write server side code, then we will get to client side and cef.

So we need to head to packages folder and there create file called `index.js`, root server file. Then create folder called `mygamemode`. 

**NOTE:
If you followed this tutorial from begining and create root `index.js` file you can skip this step.**

Next you need to open root index.js file and add
```js
require('mygamemode');
```
That's all for now for this file, you can close it and open `index.js` file that is located inside `mygamemode` folder. All the fun part starts from now on. We will start to building server side logic of gamemode.

## Commands
In this section we will write basic ragemp server side commands. First we will write command that show some value inside game chat. We will also use colors for that, which we will enable: 
```js
mp.gui.chat.colors = true;
```

### Me command

Then we can write command. If you interest in GTAV alternative multiplayers you must heard about roleplay. To do some action you use there chat with commands like: me, do, twitter, etc. Let's try `me` command for the first usage of commands.
```js
mp.events.addCommand("me", (player, fullText) => {
    player.outputChatBox(`${player.name} {#ff0000} ${fullText}`); 
})
```

This code creates command which listen for chat `/me` and some text after that. And display color text to chat `yourname text that you enter after /me`.

### Spawn car
It is simple write? Let's write something more complecated, like spawn a car command.

But before that we need to choose vehicle that we want spawn. You can get them from [wiki](https://wiki.rage.mp/index.php?title=Vehicles) page.
For basic spawn car we can use example that is shown on [wiki](https://wiki.rage.mp/index.php?title=Vehicle::Vehicle) page, we will bring it to this tutorial with some modifications as below.
```js
let vehicle = mp.vehicles.new(mp.joaat("turismor"), new mp.Vector3(-441.88, 1156.86, 326),
    {
        color: [[0, 255, 0],[0, 255, 0]],
        heading: player.heading,
        locked: false,
        engine: true,
    });
vehicle.numberPlate = "ADMIN";
```
Above script will spawn vehicle named **turismor** on coords that are inside **Vector3**. We use **Vector3** when we do something with position inside of GTAV. In ragemp it is implemented as `mp.Vector3`. Before that we use `new` keyword, because we create new object of `mp.Vector3`. 

Then for color value we pass array of two array of **RGB** values for vehicle color. First array is primary color, second array is secondary color. 

Next we pass heading, value that is current heading of player. Heading means which way is player looking at, rotation of ped that you are playing. We can also add heading to vehicle, which means we set rotation of vehicle, so in this example we will set rotation the same as player has.

Locked value set to false means that on start vehicle is unlocked, this means everyone can enter spawned car.

Engine set to true means engine will run after vehicle spawn.

If you want to add custom plate for your vehicle you should write it outside of function that creates vehicles, otherwise it will not work correctly.

Now we know how to spawn vehicle. We will use this example in our command example. Our command will check if player enter some value, if enter some value, we will spawn this car that user enter to chat, otherwise we will spawn random car that we store inside of external file. So let's get worked. 

First we will create external file name `vehicles.js`, which contains array of vehicles name that we will spawn.
```js
// vehicles.js
module.exports = [
    'club',
    'f620',
    'tribike',
    'thruster',
    'oppressor2',
    'shotaro',
    'hakuchou',
    'coquette3',
    'formula',
    'openwheel1',
    'superd',
    // Add more vehicles below.
]
```
After following this tutorial or have knowledge of Javascript you should notice that above we store data inside array. With `module.exports` syntax we create module from that array, which we can import in other files.

Next we will create `commands.js` where we will write all commands.
```js
// commands.js

// import external files
const vehicles = require('vehicles');
// me command

// spawn vehicle command
mp.events.addCommand("spawnCar", (player) => {
    let vehicle = mp.vehicles.new(mp.joaat("turismor"), new mp.Vector3(player.position.x, player.position.y, player.position.z), {
            color: [[0, 255, 0],[0, 255, 0]]
        })  
        
        vehicle.numberPlate = "ADMIN";
        vehicle.heading = player.heading;
        vehicle.locked = false;
        vehicle.engine = true;
        
        player.putIntoVehicle(vehicle, 0);
        player.outputChatBox(`${player.name} put in turismor`);
})
```

If you copy this code to your server side files and run game. After type `/spawnCar` and hit enter you should see car which spawns on your position. You can change your position and repeat this command and see wher vehicle spawns.

So this is basic car spawn that we can use inside ragemp. But this example can spawn only one car type (turismor). So what if we want to spawn some other cars with this command. Now we will modify above code to spawn different cars.

```js
// ...
mp.events.addCommand("spawnCar", (player, _, model, fullText) => {
    if (model === undefined) {
        let veh = vehicles[Math.floor(Math.random() * vehicles.length)];
        let vehicle = mp.vehicles.new(mp.joaat(veh), new mp.Vector3(player.position.x, player.position.y, player.position.z), {
            color: [[0, 255, 0],[0, 255, 0]],
            heading: player.heading,
            locked: false,
            engine: true,
        })  
        
        vehicle.numberPlate = "ADMIN"
         
        player.putIntoVehicle(vehicle, 0);
        player.outputChatBox(`${player.name} put in ${veh}`);   
    }
```

**This example is fully modified last one. Only thing that is not change is require statement and me command.**

First we need to check if player enter something to chat. If user doesn't enter anything after command name, variable of model will be undefined. If that value is undefined, so user doesn't enter vehicle model name, we will use random car name store in vehicle array that is imported in the first line of command file. 

Next we use syntax that we already know from last example to create vehicle that we want use. New values used in this and above example is `putIntoVehicle`. As the name shows this function will put player, that uses this method, inside specific random vehicle from our array of vehicles inside driver seat. 

::: tip
It is good to mark, that in 0.3+ version of RAGEMP we should use -1 to put inside driver seat. In version 1.0+ we should use -1 value to put player inside driver seat. 
:::

Now we will handle what should happend when user doesn't enter any value after command.
```js
// ...
mp.events.addCommand("spawnCar", (player, _, model, fullText) => {
    if (model === undefined) {
        // ...
    }else {
        let veh = mp.vehicles.new(mp.joaat(model), new mp.Vector3(player.position.x, player.position.y, player.position.z),
        {
            numberPlate: "ADMIN",
            color: [[0, 255, 0],[0, 255, 0]]
        });

        // ...
    }
```
This example should be really simple for you. If some value was enter to chat we spawn car with that name. 

::: tip NOTE 
In this example we use bad synax, inside vehicle creation function, for setting number plate. That won't work. To set vehicle number plate outside of this function.
:::

## Set HP
Now we will cover how to set player hp or kill player.

Very basic command for setting player hp you can find on ragemp wiki under tutorials for new developers. But we will modify this example little bit.

First lets brind that example, to see how to set player hp.
```js
// ...
mp.events.addCommand("hp", (player) => {
    player.health = 100;
})
```

If you go to [ragemp wiki](https://wiki.rage.mp/index.php?title=Player::health), you will see that `player.health` can be use as setter (set value) or get (get value that is store inside variable) to get or set value of player health or hp. Also you can see there that value of health can be from 0 to 100. This knowledge we use for setting value of player health.

Now if we know how to set player health we will modify this command that we can set player health from command.

```js
// ...
mp.events.addCommand("hp", (player, _, hp) => {
    if (hp === undefined) {
        player.health = 100
    }else {
        player.health = parseInt(hp);
        player.outputChatBox(`Player hp was ${player.health} and was set to ${hp}`)
    }
})
```
In above example we do the same check as in **spawnCar** command, we check if value that we want to set is given which means it is not undefined, has some value. 

If it doesn't have any value we set player hp to max value possible (100). Otherwise if something is passed we have that value but it is string (plain text) and value of player health requires number value. So we use parseInt method on hp value enter in chat and change string to integer (number) and set that value as player health. 

*If you don't belive what we said here you can check it yourself by using **typeof** method which we mentioned earlier in this tutorial.* Next we do simple annouce for player that setting player health went successfully.

## Kill player
If we now how to set player hp, we can see how to kill player. Basicly kill player means set his value of hp to 0, which we will do right now.

```js
// ...
mp.events.addCommand("kill", (player) => {
    player.health = 0;
})
```

Using this command will immediately kill player. Before using or testing this command note that you should implement event which will handle what happend after player death. We will do it later in events section.

### Teleport
Now we will write usefull command for server developers or admins, which is tp command. This command allows use to change our position using chat.

```js
// ...
mp.events.addCommand("tp", (player, _, x, y, z) => {
    if (parseFloat(x) === undefined || parseFloat(y) === undefined || parseFloat(z) === undefined) {
        player.outputChatBox("usage: /tp [x], [y], [z]")
    }else {
        player.outputChatBox("Player position changed").
        player.position = new mp.Vector3(parseFloat(x), parseFloat(y), parseFloat(z))
    }
})
// example /tp -441.88, 1156.86, 326
```

### Call event
On server side we can call event from another file. Below command will demostrate you how to call event from another external file. You should create `event.js` file in your gamemode server-side folder.
```js
// commands.js
mp.events.addCommand("setweather", (player, _, weather) => {
    mp.events.call("setWeather", weather);
})
```

In this example we use command to change weather with given parameter. We will get back to this example in `events.js` file.

So now let's head to `events.js` file to learn more about events. 

If you lost when following this example you can check out full code for this tutorial on [gitlab](https://gitlab.com/Czesiek2000/ragemp-tutorial).

<Edit path="gamemode.md" />