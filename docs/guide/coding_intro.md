# Javascript introduction
In this section you will get basic knowlegde of Javascript programming. RAGEMP uses javascript for creating UI and executing client-side functions to manipulate client-side of the game. Inside UI javascript is fully supported the same as in browser (in fact it is your browser rendering inside game :smile:). However client-side doesn't have access to `window, DOM etc.` object. Though CEF have access to everything the same as browser.

## Semicolon
In javascript you can use `;` at the end of for example variable declaration, but if you don't that's not bad. For example below code are all good.
```js
let a = 1; // Good
let b = 1 // Also good
```
::: tip
Sometimes code without semicolons can produce some unexpected errors.
:::
## Comments
Javascript support two types of comment: 
> Singleline comment

```js
// This is singleline comment
```

> Multiline commnet
```js
/*
    This 
    is 
    multi 
    line
    comment
*/
```

Text after `//` and between `/* */` will be ignore by compiler.

## Variables
In modern javascript you can declare two types of variables, constant that can never be change with `const` and changable values with `let`.
```js
// Old syntax 
var oldVal = 1;

// New syntax
let val = 2;
const value = 3;
```

With const you declare values that will never change like modules that you import to your resource or position to spawn player.

With let you declare values that can be change, means you can assign new values to variable declare with let.

```js
// Good
let i = 1;
i+=1;
console.log(i);

// Bad 
const i = 1;
i+=1;
console.log(i); // Error, assignment to constant variable.
```

::: warning NOTE
In first version of javascript assinging values to variable needs to be prefix with var keyword. But in new version (ES6), that RAGE is supporting, you should use above new syntax. For more info about var keyword check js docs for [var keyword](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/var)
:::

## Functions
Javascript as modern language supports functions to create cleaner, easy to read and maintain code.

Basic structure of functions in Javascript
```js
// Old version of function 
function name (parameters) {

}

// Arrow function - new version of function 
const name = (parameters) => { 

}
```

Just write function as above does nothing. To execute this function to show some results you need to write: 
```js
name(parameters)
```

Adding parameters to function are optional. But if function has some parameters to work needs to be added inside (), replacing `parameters` word.

## Arrays
In javascript we can store variable like [above](#variables), but we can store only one value for one variable name. We can't do something like:
```js
let a = 1,2,3 // Error Unexpected number 
```
To solve above problem we can use arrays.

Array is used for store multiple values with the same type, such as string, numbers, floats. If we have list of values like: 
```js
let a = 1;
let b = 2;
let c = 3;

// Use array here
let numbers = [1,2,3];

// Store variables in array:
let nums = [a, b, c]; // 1,2,3

// Store string in array: 
let strings = ['a', 'b', 'c'];

// Other way to create array with new keyword
let numbers = new Array(1,2,3);
```
We can store values in array, but how can we access that values or display array values?

```js
// To access values from array we can refer to index of that value
let one = numbers[0];
let two = numbers[1];
let three = numbers[2];
```
:::warning
In array first element has index of 0 not 1. 
Second element has index of 1
:::

We can change values in array. This example will change first value in array to 4
```js
// Syntax
numbers[positionInArray] = newValue;

// Example
numbers[0] = 4; // [4, 2, 3]
```

## Return value
Functions can return some values.
```js
// Function without parameters, return variable of value
function name () {
    const value = 1;
    return value;
}

// Return value of function can be assign to variable for future use
const nameValue = name(); // value of nameValue will be 1
```

## Objects
In javascript you also can use object to store values to get your code more cleaner. Objects are containers used for storing multiple diffrent values and methods. Objects in Javascript are equivalent to classes in other language but easier to use.
```js{19}
// Example of object usage
const person = {
    name: "John",
    age: 50,
    all: function() {
        return name + " has " + age + " years"; // Wil show: John has 50 years
    } // Here you can add , or not both ways are correct
}

// Store position in object (useful for ragemp coding)
const position = {
    name: 'Vinewood Hills',
    X: 789.6124877929688, 
    Y: 1283.61083984375, 
    Z: 360.2142333984375
}

//  Then easy get value
console.log(`X: ${position.X}, Y: ${position.Y}, Z: ${position.Z} | name: ${position.name}`) 
```
If highlighted syntax is strange to you click [here](#template-string)

To access some value of object we need to refer to this object then to variable or method we want to access.
```js
// Syntax for accessing value of object
objectName.variableOrMethod;
// Accessing the name property of person object
person.name
```

## Classes
In javascript you can also create classes like in Java, Python or C++. This is good approach when creating big resource which created only with methods can be a little bit messy and hard for others to understand your code (if you want to publish your resource to [ragemp resource page](https://rage.mp/files/)). 
In fact classes are some kind of boilerplater for real world objects.

```js
// Example of class usage
class Name {
    constructor() {

    }

    methodName() {

    }
}

const name = new Name();

```
Lets explain line by line whats going on here.

```js{1,3-5,7-9,12,13}
class Name { 
    
    constructor(param) { 
        this.param = param
    }

    methodName() {
    
    }
}

const name = new Name(param); 
name.methodName()
```
Highlighted lines means: 

:one: Declare class with name Name.

:three: Method which runs when new Name object will be created (in line 12 it runs).

:four: Assign value param to field inside class to be stored.

:seven: Method declaration.

:eight: Method body, this is basicly javascript function that we've menthioned earlier.

:one::two: Creating new object by reference, if you enter some values inside parentasies (replace params value), will call constructor, where you can assign given values to fields inside class.

:one::three: Call methodName with object reference (which here is name variable).

## Debuging
Debugging is something that no one likes, because who likes when something doesn't work and you don't know why :question:. To get know what or why something is not working as expected, we can use some functions for debugging.
::: tip NOTE
Client side from ragemp v.1.0+ has implemented client side console, but that was not tested by. There mayby some update in future after I update my knowledge of RAGEMP :smiley:
:::
### Console.log 
First way build in Javascript and working only on serverside :exclamation: is `console.log()` you can log values to serverside console (which is `ragemp-server.exe` after opening).

#### Console Object
Console is also an javascript object the same as metioned [here](#objects). To learn more about console object go to this [link](https://www.w3schools.com/JSREF/api_console.asp)


### mp.console
Above functions like **console.log** are only available on **server-side**, so how can we debug our client side code? Until version 1.0+ introduce us to **mp.console** we only have this window

<ImageLinks src="client_debug.png" alt="client debug" />

which comes when some functions at client side we wrong written or have some bugs. 
This window tells us where a bug occures (which file and which line in this file) and then after **ReferenceError** which value is wrong. Next it prints whole line where this bug occures. 

That's **mp.console** that shows in your game (only worked with 1.0+ version of game), when you click **F11**.

<ImageLinks src="mp_console.png" alt="mp.console" />

Here you can log values from client-side with :
```js
// Log info - log client side info message
mp.console.logInfo("Log info test", true, true);

// Log warning - log client side warning message
mp.console.logWarning("Warning test", true, true); 

// Log error - log client side error message
mp.console.logError("Error test", true, true);

// Log fatal error - log client side fatal error message
mp.console.logFatal("Fatal error test", true, true);
```

### Typeof 
**Typeof** can be usef when you testing values, or output if is not as expected. With this method you can check what type is value passed as parameter.
Let's assume you want to check if docs are right about getter (value that we get) of `player.vehicle`, which returns Object.

```js
let vehicle = player.vehicle;
console.log(typeof(vehicle))) // value appears in console [Object object]
```
## Template String
Template string are new way of displaying string which we would use to concatenate with variable. This syntax is support from ES6/ECMAScript 2015+.
Let's compare old and new version of concatenating strings and variables.

```js{7-9}
// Old version
var name = "John"
var age = 5
var output = name + " has " + age + " years " // John has 5 years

// New version
const name = "John"
const age = 5
const output = `${name} has ${age} years` // John has 5 years
// template strings also support math calculations, let's have a look on example
const a = 1;
const b = 2;
const output = `a + b = ${a + b}` // a + b = ${3}
```
::: tip Note
Highlighted lines are usage of new `const` keyword
:::

See how great and easy new syntax is, right. So let's use it instead of old verison.

More about template strings [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)

## Reference links
Link to javascript [docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

Link to [tutorial](https://www.w3schools.com/js/default.asp) to get started with javascript

<Edit path="coding_intro.md" />