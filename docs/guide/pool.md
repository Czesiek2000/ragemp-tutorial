# Pool
For this example I've created single repository which is scoreboard.

In ragemp we have some pools which are collections of for example vehicles, blips on the map (sign in map that shows for example police station) this collections are used on serverside, and stores all available entities of type.

## Player pool
Players pool we can use to create basic scoreboard which will show all logged in players on the server. For that we should use **mp.players** which stores data on every player on server. Then to get each player we can use `forEach` method that we learned about in the introduction. Foreach method can be used to iterate over array (which is basicly pool) and get single player that is on the server. We can use that information to create scoreboard (table that shows all players) or to copy of GTA Online join leave notification, that shows you notification when player join or leave the game. 

::: tip
This function is available on the server side, but with server and client communication we can send data back and forth from server to client.
:::

This is basic syntax of `forEach` for player pool.
```js
mp.players.forEach();
```

With this function we have access to variable that store information about each player that.
But to get access to single ped we need to use this method inside event like 

Now we will fill the `forEach` function. We will create join notification for player.

```js
// event
mp.event.add('playerJoin', (player) => {
    // foreach function
    mp.players.forEach(_player => {
        _player.notify(`${player} joined`)
    })
})
```

Easy right ?

That function shows join notification for all players including current player (so you).

If we want to exclude current player we need to add `if` statement to check if player that is not current player. This should look like this.

```js
    // event
    mp.players.forEach(_player => {
        if(_player !== player) {
            _player.notify(`${player} joined`)
        }
    })
```

::: warning
For training you can create leave notitification. Hint for you: you should use `playerQuit` event.
:::

For vehicles, blips, objects, markers and every other global variables you can also use this `forEach` method to get single item.

You can checkout every globals that are available in ragemp [here](https://wiki.rage.mp/index.php?title=Server-side_functions#Global)
## Globals
We have also access to global varialbles that defines server configuration. They are stored in `conf.json` file inside `server-files` folder.

```js
{
	"maxplayers" : 100,
	"name" : "RAGE:MP Unofficial server", 
	"gamemode" : "freeroam", 
	"stream-distance" : 500.0,
	"announce" : false,
	"csharp" : "disabled",
	"port": 22005
}
```

Here you can see example of config file. This config is store in **JSON** file, which is short for Javascript Object Notation and is represent with `.json` extension. This file should start with `{` and end with `}` and all data keys needs to be surround with **""**. Every row should end with **,** except last one.

For example
```js
// key      separator value
"maxplayers"    :        100,
```

Learn more about JSON [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON) or [here](https://www.w3schools.com/js/js_json_intro.asp). In this tutorial section about JSON is work in progress. 

## Usage
You don't need to know how to create or use JSON inside your resource. It's really simple how to use JSON and after looking at the example you should get how to use it.

To use json we need first import it and have some reference to it, so we need to store it in variable more about in the javascript introduction [here](./coding_intro.md#variables).

We have global variable called `mp.config` which is availble in all our scripts to use. This is basicly variable that store data from above `conf.json` file which is object. It gives us access to every value from that object with period notation, like in access javascript object. For example `mp.config.maxplayers` gives us access to number of max players that can join the server. And we can access with the same scheme every value from that config.

## Project
Now we will look at scoreboard project, which will show inside CEF list of all players that are playing the game. We will send data from server to client and then to browser which will render for us list of players.

We will start with server side script, because it is shorter :smile:.

```js
mp.events.add('playerJoin', (player) => {
    mp.players.forEach(_player => {
        _player.call('addPlayers', [player.id, player.name, player.rgscId, player.armour, player.health]);
        _player.call('maxPlayers', [mp.config.maxplayers]);
        _player.call('localPlayers', [mp.players.toArray().length]);
    });
});

```

This is event that will send data to player's client-side. If we send data from server we should use square brackets `[]`, but if we send one data we can skip this brackets.

We should use some kind of event to get access to `player` variable from callback function. Then we use `mp.players.forEach` function to loop over that collection. We use `_player` variable beacause we need some variable for each player that is store in our collection. You can name otherwise if you want, but it is required for `forEach` function. Then for each player that is on the server we call client side functions and pass them arguments. Values like `player.id` or `player.name` are self explained, `mp.config.maxplayers` was mentined in previous section, but we have something strange looking `mp.players.toArray().length`. 

Value of `mp.players.toArray()` is changing collection of `mp.players` which stores all players that are on the server and change to simple javascript array. Then we are `length` property of arrays to check size of array. Size fo `mp.players` is number of current logged in players on the server.

Also need to mention is that with this code will also your nickname be displayed. If you want to avoid that you need to surround all `_player.call()` with if statement like in the beginning of this page.

## Client side
Firstly we create browser window for cef. In the first line we declare variable that will store adress of our browser. In this example we will use event called `guiReady` which triggers when browser is ready and loaded. Then we check if value of `browser` variable is not undefined we assign value of `browser` varialbe to address of browser. 

::: tip
When we declare variable in **Javascript** or **Node.js** and not assigning initial value to that variable, it will be `undefined` by default.
```js
if(!browser)
```
With this `if` statement we check if value of `browser` variable is defined. If it is defined we skip it, if not we assign address of browser to that variable.
:::

```js
let browser;
mp.events.add('guiReady', () => {
    if (!browser) {
        browser = mp.browsers.new('package://scoreboard/ui/index.html');
    }
})
```

Next what we need to do is catch events that we call inside server. All the events execute functions that are defined inside browser. We send some parameters from server-side, we need to catch them inside client-side, then this parameters are required in browser and we need to send them to browser.

First event that we catch is to insert row into table. Inside this event we call browser funtion that inserts values to the table, that was send from server.

```js
mp.events.add('addPlayers', (id, _name, scId, ping) => {
    browser.execute(`insert('${id}', '${_name}', '${scId}', '${ping}')`)
})
```

Here we catch event that we inside serverside with data from server config file. Then we execute function that is declare inside browser, which catches parameter from server and pass it to browser.
```js
mp.events.add('maxPlayers', (max) => {
    browser.execute(`max('${max}')`);
})
```

Send to browser number of players that are logged in to server, that was send to client from server.

```js
mp.events.add('localPlayers', (value) => {
    browser.execute(`local('${value}')`);
})

```

To add something to to the browser we need to call function that is created inside browser, if we want to add some values that was send we can catch them in the callback function. That's what we're going to do. 

Browser function that add server data inside table row. This function everytime it is called it grabs body of the table and create table row, where insert data that was send from the server.

```js
function insert(id, name, scId, ping) {
    document.querySelector('.table tbody').innerHTML += `<tr><td>${id}</td><td>${name}</td><td>${scId}</td><td>${ping}</td></tr>`
}
```

Now we will handle displaying our scoreboard. We can do this by listening on click inside game or use browser build in listeners for displaying scoreboard. We will use browser functionality to toggle scoreboard visibility. Everytime `z` button is clicked we check if it is hidden, then we set it to be visible, if it visible we hide it.
```js
document.addEventListener('keydown', (e) => {
    if (e.key === 'z') {
        if (document.querySelector('.main').style.display === 'none') {
            document.querySelector('.main').style.display = 'block';
        }else {
            document.querySelector('.main').style.display = 'none';
        }
    }
})
```

Here is other function that insert number of max players to the browser. It gets container from the page that was prepared in the HTML and insert value of max players to this container.
```js
function max(max) {
    document.getElementById('max-players').innerText = max;
}
```

This function insert number of currently logged in players on the server.
It does and work exactly the smae as above function. 
```js
function local(max) {
    document.getElementById('player-count').innerText = max;
}
```

We will skip HTML and CSS part in this section of the tutorial. If you want to view full source code of this project, you can check [this](https://gitlab.com/Czesiek2000/scoreboard) repository. 

<Edit path="pool.md" />
