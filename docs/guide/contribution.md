# Contribution
This section will show you how you can contribute to this tutorial and **RAGE Multiplayer**. If you learned something from this tutorial and want to try your skills in practice you can contribute to ragemp by creating some resources or to **RAGEMP Wiki** by adding some examples to unfinished sections. 

Wiki page has a lot of pages, but not all of them have examples how to use each function. There is a list of pages that needs a code examples. For more information look at *Looking forward to help us?* section in the main page of **RAGEMP Wiki**.

## Tutorial contribution
If you want to contribute to this project with writing new tutorials, you need to have installed **Nodejs** on your computer to build this project localy and have of course some knowledge of **RAGEMP API** :sweat_smile:. Also you might need **git** (version control tool) to publish your work to repository to be live and available for others to view. 

This tutorial is build on **Vuejs** and **Vuepress**, but if you want to write only content, you need to know how to create and write **markdown** files. Each of new file should be also added to `config.js` file to `sidebar` array.

### Instation
First you need to clone this repository. To do this you need to have installed **git** on your computer. You can get it from [here](https://git-scm.com/downloads).

To clone this repository use: 
```
git clone https://gitlab.com/Czesiek2000/ragemp-tutorial.git
```

Then install dependencies.
```bash
npm install

# Short command
npm i
```

Run development server. This will create local development server that should run on port **8080**. So you need to open your browser and head up to [localhost:8080](http://localhost:8080/ragemp-tutorial/).

```bash
npm run dev
```

Build for production (You don't need to use it on your local machine).
```bash
npm run build
```

If you finish your work you should add file to stage using:
```
git add file-you-work-on
```

Then commit files that you added with:
```
git commit -m "<your-commit-message>"
```

If you finish your work and commit files, you should pushed them to the remote repository to be live and available for other users. To do that you need to use:

```
git push
```

There might be some differences between your local repository and remote repository. To update local files you need to do:

```
git pull 
```

If you want to change some structure of this code you should have some knowledge of [vuejs](https://vuejs.org/) and [vuepress](https://vuepress.vuejs.org/).

:::danger
It is prefered to track every change in this tutorial. So if you want to add some content in multiple files you should add them in separated commits, rather that commiting all of them in one commit.
:::

## RAGEMP contribution
There can be two ways of contribution to RAGE Multiplayer
* you can create some resources for comunity
* you can help with answer other questions on forum

If you want to create some resources for community, you can post it on *github* or other git hosting platforms like *gitlab, bitbucket, etc.* and on ragemp forum. You should include instruction about what it is doing, how to install it, mayby some preview on how it looks in game, you can choose between image and some video. 

You should also provide information if it is client or server side resource.

For simplify for others installing your resource you should make your resource with structure
```bash
your resource name
├── client_packages            
│   ├── your resource name      
│       ├── ui                  
│           ├── images             
│           └── index.html      
│           └── script.js
│           └── styles.css
│       └── index.js            # Main index.js client side file
│   ├── packages      
│       ├── your resource name  
│           └── index.js            # Main index.js server side file

```

::: tip NOTE

If you want to install some existing resource, they will have this structure. So to install it you need to copy all content from `client_pacakes` and `packages` folder and place it in your `server-files` appropriate folders. Then you should add `require('<resource_name>')` inside root of `client_packages` and / or `packages` folder, depending what resource you are installing and what for it is created. You need to change `resource_name` to name of resource that you are installing on your server.

:::

## Wiki contribution

If you want to try other contribution you can contribute to RAGEMP [wiki](https://wiki.rage.mp/index.php?title=Main_Page). 

Here are listed some options where you can contribute to RAGEMP project.
* Categorize Pages on the Wiki: [How to Categorize Pages](https://wiki.rage.mp/index.php?title=Help:How_To_Categorize_Pages).

* Finish documentation for [Incomplete stuff](https://wiki.rage.mp/index.php?title=Category:Incomplete_Functions).

* Add examples to the Functions/Events instruction how to do this [here](https://wiki.rage.mp/index.php?title=Category:TODO:_Example).

* Write tutorials to help newbies :smile:.

* There are some pages that requires rewrite where you can check yourself. Instruction can be found [here](https://wiki.rage.mp/index.php?title=Category:Rework_required)

* You can also update assets like peds, vehicle, weapon models after new dlc being released.

<Edit path="contribution.md" />