# Native UI
We cover how to make plain client side and server side development. Now we will look how to make some gui menu GTA Online based but without using CEF. 

For that we will use library called **NativeUI** which is ported to RAGEMP by [GamingMaster](https://rage.mp/profile/18996-gamingmaster/). 

Here you can also see preview of how NativeUI looks like in RAGEMP.

<ImageLinks src="nativeui.png" alt="example" />

But before we start developing simple menu, we need to have NativeUI library inside our resource, which you can download from [here](https://rage.mp/files/file/41-nativeui/) or from gitlab repo source [files](https://gitlab.com/Czesiek2000/ragemp-tutorial/-/tree/master/code_samples) of example.

Next we need to create inside `server-files/client_packages` empty folder called `menu_sample`. Then place NativeUI library file and name it `nativeui.js`. Next create main `index.js` file where we will write all logic for our menu.

Next you need to require NativeUI library inside our main client side file.

So our folder structure which we created should look like this
```bash
.
├── clinet_packages          # Client side folder
│    ├── nativeui            # Nativeui folder
│       └── nativeui.js
│       └── index.js
│    ├── ...                 # Other client side folders
│    └── index.js  
```

To start working with this library we can go to NativeUI [docs](https://hackmd.io/@YtrpXvh-TjuZCyejHEq9Xw/HJO_CdHaM?type=view) website and get example code, which should look like below and will be included in gitlab [samples](https://gitlab.com/Czesiek2000/ragemp-tutorial/-/tree/master/code_samples)

## Code sample
```js{1,2,13,14,16,18,20,26,34,40,50,54,60}
const NativeUI = require("./nativeui/nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem; // Add single menu item
const UIMenuListItem = NativeUI.UIMenuListItem; 
const UIMenuCheckboxItem = NativeUI.UIMenuCheckboxItem; // Add box to right into menu item
const UIMenuSliderItem = NativeUI.UIMenuSliderItem;
const BadgeStyle = NativeUI.BadgeStyle;
const Point = NativeUI.Point; // Set where menu should be placed
const ItemsCollection = NativeUI.ItemsCollection;
const Color = NativeUI.Color;
const ListItem = NativeUI.ListItem;

mp.gui.cursor.visible = false;
mp.gui.chat.show(false);

const ui = new Menu("Test UI", "Test UI Subtitle", new Point(50, 50));

ui.AddItem(new UIMenuItem("Test item", "Test menu description"));

ui.AddItem(new UIMenuListItem(
	"List Item",
	"Fugiat pariatur consectetur ex duis magna nostrud et dolor laboris est do pariatur amet sint.",
	new ItemsCollection(["Item 1", "Item 2", "Item 3"])
));

ui.AddItem(new UIMenuSliderItem(
	"Slider Item",
	["Fugiat", "pariatur", "consectetur", "ex", "duis", "magna", "nostrud", "et", "dolor", "laboris"],
	5,
	"Fugiat pariatur consectetur ex duis magna nostrud et dolor laboris est do pariatur amet sint.",
	true
));

ui.AddItem(new UIMenuCheckboxItem(
	"Checkbox Item",
	false,
	"Fugiat pariatur consectetur ex duis magna nostrud et dolor laboris est do pariatur amet sint."
));

ui.ItemSelect.on(item => {
	if (item instanceof UIMenuListItem) {
		console.log(item.SelectedItem.DisplayText, item.SelectedItem.Data);
	} else if (item instanceof UIMenuSliderItem) {
		console.log(item.Text, item.Index, item.IndexToItem(item.Index));
	} else {
		console.log(item.Text);
	}
});

// ui.SliderChange.on((item, index, value) => {
// 	console.log(item.Text, index, value);
// });

ui.MenuClose.on(() => {
    mp.gui.chat.show(true);
    mp.gui.cursor.visible = false;
});


mp.keys.bind(0x71, false, () => { // F2
	if (ui.Visible) ui.Close();
	else ui.Open();
});
```

Description of highlighted lines:

:one: Import NativeUI library. 

::: tip 
To import NativeUI library we need to pass relative path, but we need to start from root `client_packages` folder where main `index.js` file is located.
:::

:two: Initalizition of NativeUI library and other stuff that come build in inside this library.

:one::three: Disable cursor 

:one::four: Disable chat to be visible

:one::six: Create native ui menu instance. 
* First parameter is big text on blue field
* second parameter is description that will display below that banner
* third parameter is where on screen will this menu be located.

:one::eight: Add item to menu. Item is basicly each row that appears inside menu.

:two::zero: This adds list item to menu. If you run this code on your server you will see that if you focus on second item you will be able to select with left / right arrow each value from items collection

:two::six: This add slider item to menu

:three::four: This add checkbox to the item. Basicly this is white square on the left in the third menu row 

:four::zero: This function checks what happen menu item is clicked. First if check if item is list item, other condition checks if clicked item is slider item. Third condition goes when clicked item is not slider or list  

:five::zero: This commented code goes when you select slider item and shuffle between each state of slider, you can do this with left / right keyword.
:five::four: This code run when you click backspace, menu disapear, chat will apear and we want to be sure that cursor doesn't show.

:six::zero: This function handle open menu when F2 key is clicked. 

If you want to use another key you can check all keycodes [here](https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes).

## Creating menu
Now when we look at sample code of NativeUI we can write our own code. First we will handle client side of native ui, then we will handle serverside events which we've called. 

### Client-side
```js
// ... imports
```
First line are all imports that native ui requires to run properly.
```js
const ui = new Menu("Test UI", "Test UI Subtitle", new Point(50, 50));
```
Next we initalize new menu instance. It requires three arguments from us to pass.
* First argument is menu title that will show inside the blue box.
* Next is description of menu, this should appear under the blue box
* Third argument is used to tell native ui where to place on the screen our menu. This value will place menu in the top left corner of screen.
```js
ui.AddItem(new UIMenuItem("Spawn car", "Spawn turismor"));
ui.AddItem(new UIMenuItem("Lock", "Lock vehicle that was spawned"));
ui.AddItem(new UIMenuItem("Heal", "Heal player"));
```
Here we add two items to menu. Menu item requires two parameters, which are:
* First value is label that will show on the menu item
* Second value is description of item, when we hover that item we should see box in the bottom of menu that will show that description.
```js
let item1 = new UIMenuListItem(
	"Weapons",
	"Give player specific weapons",
	new ItemsCollection(["weapon_grenade", "weapon_assaultsmg", "weapon_advancedrifle"])
);

ui.AddItem(item1);
```
There is also awailable another way to add item to **NativeUI** by firstly assigning item variable and then add it to menu container with referencing to variable. This will be helpfull when we want to check if item was clicked, which you can see below.

So there are two ways of adding item to menu by declaring item as variable and add to menu or simply using method `AddItem` and create new **MenuItem** inside which will be added to menu container.

::: tip
There is also way to assing value inside `AddItem` function like this
```js
mainMenu.AddItem(item = new UIMenuItem("Test", "Test description"));
```
:::

```js
ui.ItemSelect.on((item) => {
	if (item.Text === "Spawn car") {
		mp.events.callRemote('spawnCar');
	}else if (item.Text === "Lock") {
		mp.events.callRemote('lockVehicle');
	}else if (item.Text === "Heal") {
		mp.events.callRemote('healPlayer');
	}
})
```
Here we call NativeUI method `ItemSelected` which checks which item was clicked with `Enter` key and run **if** statement which check value of text inside that item if match to given conditions. If something match server side event is called.

```js
ui.ListChange.on((item, index) => {
	switch (item) {
		case item1:
			mp.events.callRemote('playerWeapon', item.SelectedItem.DisplayText)
		break;
	}
})
```
This function is listening for arrow changes on item collection. If you select one value and click enter this should give you selected weapon. 

```js
ui.MenuClose.on(() => {
    mp.gui.chat.show(true);
    mp.gui.cursor.visible = false;
});
```
This function is copied from **NativeUI** example. This function handles menu close after exiting with **Backspace** key.

```js
mp.keys.bind(0x71, false, () => { // F2
	if (ui.Visible) ui.Close();
	else ui.Open();
});
```
Here we binding keys, which handles what to do when key is clicked. In our example we want to open menu when specific key is pressed. We use build in RAGEMP function which handles specific button press. Value of `0x71` is virtual key which represents **F2** key from keyboard. 

Whole list of key codes you can find [here](https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes).

Checkout full code of this clientside example [here](https://gitlab.com/Czesiek2000/ragemp-tutorial/-/tree/master/code_samples/client_packages//menu_sample/index.js)

### Server side
Now we will cover server side events that we called from client.

```js
let vehicle;
mp.events.add('spawnCar', (player) => {
    vehicle = mp.vehicles.new(mp.joaat("turismor"), new mp.Vector3(-441.88, 1156.86, 326),
    {
        numberPlate: "ADMIN",
        color: [[0, 255, 0],[0, 255, 0]]
    });

    vehicle.numberPlate = "DEV";
})
```
In first line we declare `vehicle` variable, which will be used later to lock and unlock spawned vehicle.

Next we catch event called `spawnCar` with one parameter player, that run this event. Then we create new vehicle, which we know from previous tutorial section.

::: tip NOTE
If you want to spawn with native ui car there are two important things:
1. You do this with client side only just catch this event on clientside or simply spawn car inside if condition.
2. To spawn specific car that was selected you can pass it as parameter to server or client the same as in giving weapon to player.
::: 

**Note: This can be done inside client side and send status to server**

```js
mp.events.add('healPlayer', (player) => {
    player.health = 100;
    player.outputChatBox('player was healed');
})

```
Here we also catch event that we called from client, which after click on menu will heal player. This event should be familiar from previous sections

```js
mp.events.add('lockVehicle', (player) => {
    _locked = false;

    _locked = !_locked;

    vehicle.locked = locked;

    player.outputChatBox(`vehicle ${_locked ? "locked" : "unlocked"}`);
})

```
Here we catch event that should lock our vehicle that we spawned. First we declare variable that will store state of lock vehicle. 

Then we do something called **toggle** value, which means change the state of variable. If value is `true` then we set to `false` and backwards if value is `true` we set it to `false`. In our case we toggle value of `vehicle.locked`, which stores value of car locked state on server side. 
In simpler words toggling value, means if vehicle is lock (`vehicle.locked` is set to `true`) unlock it (set `vehicle.locked` to `false`) and if is unlocked locked it (set `vehicle.locked` backwards as ).

We change the state of `_locked` variable from `true` to `false` and backwards from `false` to `true`. In simple words this means we get value _locked and if it is **false** we changed it to **true**, or if it is **true** we change it to **false**. Then we assign that value to `vehicle.locked` which handles vehicle lock state on server.

Then we assign that variable to `vehicle.locked` which holds state of vehicle lock.

Next we output to chat message to chat for player to know what lock state is vehicle.

```js
mp.events.add('playerWeapon', (player, weapon) => {
    player.giveWeapon(mp.joaat(weapon), 9999);

    player.notify('Player get weapon');
})
```
Here we catch event that we've send from client side. This event takes two parameters. First is player, as always on serverside, which handles this event. Second is weapon which we select from menu. 

Then we call `giveWeapon` for player that handles this event. This functions takes two parameters. First is weapon that we want to give to player, it requires from us to use hash code which looks like this `0x92A27487`, or use `mp.jooat` function that changes string weapon name to hash code.

After you've done everything correctly you can launch the game and you should see in the top left corner new menu. Your new menu should look like this: 

<ImageLink src="menu_preview.png" alt="menu_sample" />

You can checkout full code example of the server side part [here](https://gitlab.com/Czesiek2000/ragemp-tutorial/-/tree/master/code_samples/client_packages//menu_sample/index.js)

:::tip 
If you add line 
```js
ui.Close();
```
you should first press key which opens menu (in our case is F2), otherwise menu will open automaticly on game start and wait until you close it with **backspace**, key that you opened it or reset your server. 
:::

For training after understanding this section you can implement close button on menu, which after click will close menu.

<Edit path="nativeui.md" />