# Nodejs Server-side
Nodejs is used to create server side scripts and resources. It has the same syntax as Javasript which was mentioned [here](coding_intro). The only addition is that thanks to Nodejs we can separate our resource into multiple files and import them to main file.

## Require
**Require** is keyword that we use to **import** files (modules) that we create. We can **exports** arrays, objects, functions. Require is build in Nodejs core library which is used in server-side. 
Syntax is really simple and looks: 
```js
// As always in this tutorial replace <> with your values.
require("<your-filename>");
```

But to require module we need to create it. To do this, we need to do: 
```js
// moduleName.js 
let array = [1,2,3];
module.exports = array;

// After that use above require function

// index.js
require('./array');
```
::: tip
Above **require** method can be used for client-side manipulation. For example importing files to root `index.js` file.
:::

## Modules
There was mentioned before that RAGEMP scripts can be made using separate files called modules. We will now focus on modules, how they work and how to use them.

Assume that we have to store multiple data like ped skins, vehicle models and weapons model. We can store them in array, which we covered [here](coding_intro.md#arrays), as you can see below.

```js
const skins = [
    'mp_m_freeemode_01',
    'player_zero',
    'player_one',
    'player_two',
    // And lots more
]

const vehicles = [
    'adder',
    'turismor',
    't20',
    'cheburek',
    // And lots more
]

const weapons = [
    'weapon_grenade',
    'weapon_grenadelauncher',
    'weapon_carbinerifle',
    // And lots more
]
```

More data can be found on specific wiki pages: [peds](https://wiki.rage.mp/index.php?title=Peds), [weapon](https://wiki.rage.mp/index.php?title=Weapons), [vehicles](https://wiki.rage.mp/index.php?title=Vehicles) 

We can separate them into few files. We can do it on server side with modules. More on node modules [here](https://nodejs.org/api/modules.html). Let's look at example.

```js
// skins.js
const skins = [
    'mp_m_freeemode_01',
    'player_zero',
    'player_one',
    'player_two',
    // And lots more
]

module.exports = skins;

// vehicles.js
const vehicles = [
    'adder',
    'turismor',
    't20',
    'cheburek',
    // And lots more
]

module.exports = vehicles;

// weapons.js
const weapons = [
    'weapon_grenade',
    'weapon_grenadelauncher',
    'weapon_carbinerifle',
    // And lots more
]

module.exports = weapons;

// index.js
require('./skins'); // !! Important: YOU NEED TO ADD ./ BEFORE MODULE NAME
// We can also import like this:
const vehicles = require('./vehicles'); // With this syntax we can for example loop over data stored in array of vehicles
require('./weapons');
```

To clear everything we use 
```js
module.exports = nameOfExportedValue
```
To export value from file and require in main `index.js` file on server side.

## Modules from one file
We can also create multiple modules from one file. We need to use another syntax to do this. We will use data from previous example for this example.
```js
// all.js
const skins = [
    'mp_m_freeemode_01',
    'player_zero',
    'player_one',
    'player_two',
    // And lots more
]

const vehicles = [
    'adder',
    'turismor',
    't20',
    'cheburek',
    // And lots more
]

const weapons = [
    'weapon_grenade',
    'weapon_grenadelauncher',
    'weapon_carbinerifle',
    // And lots more
]

// Exports 
module.export.skins = skins;
module.export.vehicles = vehicles;
module.export.weapons = weapons;

// Or export like this:
module.exports = {
    skins,
    vehicles,
    weapons,
}

// index.js
const { skins, vehicles, weapons } = require('./all.js'); // Import all modules to index.js for future usage assign to variable.
```

<Edit path="node.md" />
