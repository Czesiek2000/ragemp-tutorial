# Creating resources
RAGEMP server resource is easy to create. 
RAGEMP when compiling server files looks inside `server-files` for `index.js`.
Resources are folders that are created in `server-files` folder.
To include resource for compilation add to root `server-files` `index.js` file line: 
```js
// main index.js
require('./<resource-name>'); // Replace <resource-name> with name of your resource.
```

## Making resource
Go to `server-files` and create folder with name that you want resource to be named. When resource folder is created go to that folder and create `index.js` file. Repeat this steps everytime you create new resource.
This `index.js` file will be your main resource file you can write here all your code or create other files and `require('')`. After that `require('')` your main `index.js` file in root `index.js` file.

::: tip
When creating resource in one `index.js` file your code may get messy when resource will get bigger, so better approach is to create gamemode in separate files and `require()` all in main `index.js` file.
:::

## Client side resource 
Client side compilation works the same as server files. 
Inside client_packages create `index.js` file that will be root client-side file that holds reference to all client-side resource. Then create folder with name that you want your client side resource to be named, open root index.js in editor and `require('<client-resource-name>')`, create inside `index.js` `client_packages/<client-resource-name>`, open it inside code editor and here will be written client side code


## File tree

```bash
.
├── packages              # Folder for server files
│   ├── resource          # Folder where your resource wil be saved 
│       ├── index.js      # Resource index file
│   └── index.js          # Root server files js file
│   ├── ... 
├── client_packeges       # Folder for server files
│   ├── resource          # Folder where your resource wil be saved 
│       ├── index.js      # Resource index file
│   └── index.js          # Root server files js file

```

## Autocomplete
### Server side
It is worth metioned that ragemp has autocomplete module for easier development. For this to work you need to have nodejs installed. If you don't have you can download it from [here](https://nodejs.org/en/). You can choose latest version that is stable and recommend for most users, or you can choose development version.

If you have installed nodejs you can open your terminal inside **packages** folder under **server-files** and type following command.

```
npm install --save-dev github:CocaColaBear/types-ragemp-s#master
```

That will add to your project types. You can see if it works by typing for example `mp.` after that something like this should apear

<ImageLinks src="autoc.png" alt="autocomplete" />

Then select what you want to add after `mp.` and hit **TAB** to choose selected property.

Or assume that we want to create **Vector3**. So start typing `mp.Vector3` and hit **TAB**. Then you should see something like this.

<ImageLinks src="autoc1.png" alt="autoc" />

And with that you see what parameters this function requires.

### Client side
We can also add autocomplete to client files. This is basicly the same as setting up autocomple for server side.

Good tutorial how to setup this autocomplete can be found 

* On ragemp forum thread [here](https://rage.mp/forums/topic/5103-part-1-setting-up-a-development-environment-nodejsvsc/) 

* Or on ragemp wiki [here](https://wiki.rage.mp/index.php?title=Getting_Started_with_Development) inside **Setting up API auto complete (TypeScript)** section

<Edit path="resource.md" />