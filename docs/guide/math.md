# Javascript Math
Javascript as modern language also supports mathematical operations, such as addition, subtraction, division, and so on.

Example of basic mathematical operations in Javascript:

```js
let a = 2;
let b = 3;
let c = a + b; // 5
c = a - b // -1
c = a * b // 6
c = a / b // 0.6666666666666666
```

## Random value
To get random value we need to call `random` method from **Math object**. It will give us value between **0** and **1** such as **0.5049721452376315** this is :smile:
```js
Math.random(); // Really simple right ? 
```

## Rounding random value
Value of `Math.random` contains decimal. We want to get value of **0** or **1** not **0.5049721452376315**. So what we should do ? 

Answer is really simple, we need to call another method from **Math object** called `floor`. So let's do it, shall we ?

```js
Math.floor(Math.random()) // That wil round values of Math.random, but this values are below 1 so this will always be 0.

Math.floor(Math.random() * 2) // This will give us result 0 or 1.
```

We now know Math.random and Math.floor method so let's combine them and use in practical example.

Assume we don't want value between 0 and 1, but 0 and 255 to create random color which should look like this [random,random,random]. We need three random values so we need three variables for this.
```js
    let random = Math.floor(Math.random() * 256);
    let random2 = Math.floor(Math.random() * 256);
    let random3 = Math.floor(Math.random() * 256);

    let color = [random, random2, random3];
```

## Remove decimals
When we are covering math in Javascript, it is worth mention useful method for removing decimal values of number, for example we have number 0.17960341317540252 we can use method `toFixed`. 
```js
let rand = 0.17960341317540252;
rand.toFixed(); // 0
rand.toFixed(1) // 0.2
rand.toFixed(2) // 0.18
```

This method will delete decimals based on paramether inside `()`. If you look at this example, when we enter 1 in `()` we see value of 0.2, not 0.1. That's because this method also round result values. So if next values of value in `()` that we passed as paramether is bigger that 5 it will round it up, if not it will round down.

More on toFixed Javascript method you can read [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toFixed)

## Math reference and more informations
If you want to learn more about Math object, checkout this [link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math)

<Edit path="math.md" />