# Comunication
In ragemp we can comunicate between client and server. Basicly this means we can send data from server to client. Also with this communication we can trigger some client side events that we have registered. 

:::warning
Without triggering client side event, with for example server side chat command, nothing will happen. 
:::

Another reason why important is using this communication is to open CEF browser. 

Now we will look at some examples of communication in ragemp.

```js
// commands.js
mp.events.addCommand("sound", (player) => {
    player.call("playSound");
});


// client_packages/mygamemode/index.js
mp.events.add('playSound', () => {
    mp.game.audio.playSound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", true, 0, true);
})

```

We registered command which after executing calls client side event, which for us will play game sound. To call client side event we use `player.call`, then we register client side event which executes `playSound` function. 

This function will play build in game sound. More about **playSound** [here](https://wiki.rage.mp/index.php?title=Audio::playSound). In this example I've used example from the wiki page of **playSound**. If you visit wiki page you will find description of each parameter and and link where you will find more sound names to use.

## Client communication
We can also call server side event from client side. Calling server side event from client side we can do with `mp.events.call`. 

For this example we will use client side implementation of `mp.events.addCommand()` for register chat command on client side.
```js
// client_packages/mygamemode/index.js
mp.events.add("playerCommand", (command) => {
	const args = command.split(/[ ]+/);
	const commandName = args.splice(0, 1)[0];
		
	if (commandName === "calltest") {
		mp.events.call("test");
    }
    
});

```
We registered client side command `/calltest`. We check here if command name is **calltest**. If it is true we call serverside event called **test**. 

```js
// packages/mygamemode/mygamemode/events.js
mp.events.add("test", (player) => {
    player.outputChatBox("Test command");
})
```
Next we register **test** event on server side inside `events.js` file. This event catch call from client from **calltest** event which we registered above. Next we output to chat message to test if everything went well.

## Call server
We can call serverside event and send data back to server from client.
For this example we will call serverside event when we catch client side event inside **test** event. 

Above event needs little modification.

```js
mp.events.add("test", (player) => {
    // ...
    player.call("testreceive")
})
```

Then we need to catch this event inside client.
```js
mp.event.add("testreive", () => {
    player.outputChatBox("test received")
})
```

## Send data
When we register and call event we can pass some data between server and client. That is useful for example to create hint notification, when we trigger some action, for example enter some coords display hint to press button, or just after command is triggered.

This is preview what we want to achieve

<ImageLinks src="help_notification.jpg" alt="help notification" />

Let's write some code
```js
mp.events.addCommand("hint", (player) => {
    player.call("hintmsg", ["Hit ~INPUT_CONTEXT~ to do something"])
})
```
First we will create command that will trigger that event. Inside that command we call clientside event that display hint message. 

For function `player.call` can use two arguments. First is function name which is mandatory. Second is value that we want to send to client. 

:::tip
Send values need to be surround with **[ ]** and have **" "** for string. If you want to send more than one parameter just use **,** after each parameter.
:::
```js
mp.events.add("hintmsg", (msg) => {
    mp.game.ui.setTextComponentFormat('STRING');
    mp.game.ui.addTextComponentSubstringPlayerName(msg);
    mp.game.ui.displayHelpTextFromStringLabel(0, true, true, -1);
})
```

Next we catch event that we send from server. To connect them logically I named it **hintmsg**, so then I know that for **hint** event that send message I should look for **hintmsg** event in another file. 

You may wondering how our text arrived to client. Unswer is really simple, this is `msg` value that is between **()**. 

For above example I use code from [wiki](https://wiki.rage.mp/index.php?title=Ui::displayHelpTextFromStringLabel).

This notification should after short amount of time disapear automatically. But if we want to do it faster we could do it ourselves. 

```js
mp.events.add("hintmsg", (msg) => {
    //...
    setTimeout(() => {
        mp.game.ui.clearHelp(true);
    }, 3500)
})
```
Here we monify above event and add timeout. After reading [this](timing#settimeout-syntax) section you should already know that after 3.5 second we will call function `mp.game.ui.clearHelp()`, with argument `true` which will remove this help message from screen.

:::tip
This can be also done on serverside, with registering other event and then call client side with `player.call()`
:::

You can try for training change this example and do it with serverside.

You can find source code from this tutorial [here](https://gitlab.com/Czesiek2000/ragemp-tutorial/-/tree/master/code_samples) and navigate to correct files.

<Edit path="communication.md" />