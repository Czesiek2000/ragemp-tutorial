# Tutorial references and link
* [Wiki](https://wiki.rage.mp/index.php?title=Main_Page) tutorial pages, functions pages, events pages, peds, weapons, vehicles, some images
* [Forums](https://rage.mp/forums/)
* [JS docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
* [Node docs](https://nodejs.org/api/modules.html)
* HTML
    * [HTML tutorial](https://www.w3schools.com/html/)
    * [Another HTML tutorial](https://www.tutorialspoint.com/html/index.htm)
* CSS
    * [CSS tutorial](https://www.w3schools.com/css/) 
    * [Another CSS tutorial](https://www.tutorialspoint.com/css/index.htm)

* [JS events](https://wiki.rage.mp/index.php?title=Server-side_events)

* [Source code](https://gitlab.com/Czesiek2000/ragemp-tutorial/-/tree/master/code_samples)

* [Some of my code snippets](https://gitlab.com/users/Czesiek2000/snippets) - This is collection of snippets from my own ragemp research

* [CEF](https://en.wikipedia.org/wiki/Chromium_Embedded_Framework)

## Thanks
I want to thank RAGEMP developers for good written wiki page and awesome game.
Also thanks to RAGEMP for providing awesome resource that I can learn from and create this tutorial.
You dear reader / future RAGEMP developer for reading this tutorial.

## Policy
Below is RAGEMP 1.0+ policy screenshot from game.

<ImageLinks src="ragemp_policy.png" alt="policy" />

<Edit path="reference.md" />