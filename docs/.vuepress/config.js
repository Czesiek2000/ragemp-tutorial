require('dotenv').config()
const webpack = require('webpack')

module.exports = {
    title: 'RAGEMP Tutorial',
    base: '/ragemp-tutorial/',
    dest: 'public',
    description: 'RAGEMP tutorial for newbies',

    themeConfig: {
      head: [
        ['link', { rel: 'shortcut icon', href: '/favicon.ico' }]
      ],
        logo: '/img/logo.png', 
        sidebar: 'auto',
        sidebarDepth: 2,
        displayAllHeaders: true,
        smoothScroll: true,
        searchPlaceholder: 'Search...',
        lastUpdated: 'Last Updated',
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Guide', link: '/guide/' },
            { text: 'Gitlab', link: 'https://gitlab.com/Czesiek2000/ragemp-tutorial' }
        ],

        sidebar: [
          '',
          '/guide/',
          '/guide/start',
          '/guide/coding_intro',
          '/guide/loops_and_conditions',
          '/guide/timing',
          '/guide/math',
          '/guide/fileStructure',
          '/guide/conf',
          '/guide/resource',
          '/guide/commands',
          '/guide/events',
          '/guide/node',
          '/guide/client_events',
          '/guide/client_exports',
          '/guide/cef',
          '/guide/gamemode',
          '/guide/event',
          '/guide/communication',
          '/guide/nativeui',
          '/guide/pool',
          '/guide/contribution',        
          '/guide/reference'
        ]
      },
      markdown: {
        lineNumbers: true
      },
      plugins: [
        [ '@vuepress/back-to-top', ],
        [ '@vuepress/plugin-medium-zoom' ],
        [ '@dovyp/vuepress-plugin-clipboard-copy', true ],
        ['@vuepress/active-header-links'],
        [
          'google-analytics-4',
          {
            // your gtag tracking ID
            gtag: process.env.GOOGLE_ANALYTICS
          }
        ],
		    [ 'vuepress-plugin-google-adsense', { 'google_ad_client': process.env.AD_CLIENT, 'enable_page_level_ads': true } ],
    ],
	configureWebpack: (config) => {
		return { plugins: [
		  new webpack.EnvironmentPlugin({ ...process.env })
		]}
	  }
}
