// Mazebank top
let spawnPos = {
    X: -71.9208,
    Y: -817.9440,
    Z: 326.0485
}

// Handle event when user open game and log to server 
mp.events.add('playerJoin', (player) => {
    player.model = 'mp_m_freemode_01'; // Set player model
    player.spawn(new mp.Vector3(spawnPos.X, spawnPos.Y, spawnPos.Z)) // Spawn player on specific position which we store insdie spawnPos object
    player.notify(`~b~ ${player.name} ~w~spawned`) // Notification above map
})

// Handle event when player is dead
mp.events.add('playerDeath', (player) => {
    player.spawn(new mp.Vector3(spawnPos.X, spawnPos.Y, spawnPos.Z)) // Spawn player on specific postion after death
})

// Handle event when player quit or leave game.
mp.events.add("playerQuit", (player) => {
    player.notify(`${player.name} leave the server`) // Set notification above map announce that player quit game
})

// Handle event that we call inside commands.js
mp.events.add("setWeather", (player, weather) => {
    mp.world.weather = weather;
    player.outputChatBox(`Weather changed from ${mp.world.weather} to ${weather}`);
})