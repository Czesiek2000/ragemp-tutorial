const vehicles = require('./vehicles');
const peds = require('./peds');
mp.gui.chat.colors = true; // enable colors in chat
mp.events.addCommand("me", (player, fullText) => {
    player.outputChatBox(`${player.name} {#ff0000} ${fullText}`); // color chat 
})

// Command to spawn car, example: /spawnCar adder
mp.events.addCommand("spawnCar", (player, _, model, fullText) => {
    // Check if value of model is empty
    if (model === undefined) {
        // Assign random vehicle to veh value.
        let veh = vehicles[Math.floor(Math.random() * vehicles.length)];
        // Create and spawn this random vehicle.
        let vehicle = mp.vehicles.new(mp.joaat(veh), new mp.Vector3(player.position.x, player.position.y, player.position.z), {
            color: [[0, 255, 0],[0, 255, 0]], // Set vehicle color
            heading: player.heading, // Set vehicle rotation
            locked: false, // Set if vehicle should be locked when spawn or not (change to true to lock vehicle)
            engine: true, // Set if vehicle engine should run on start
        })  
        
        vehicle.numberPlate = "ADMIN" // Set custom text on spawned vehicle  
        
        // Check if player is inside vehicle, if true despawn it, and spawn another vehicle
        if (player.vehicle) {
            player.vehicle.destroy(); // Remove vehicle that player is currently in
        }
        
        player.putIntoVehicle(vehicle, 0); // Put player inside random vehicle that we spawn.
        player.outputChatBox(`${player.name} put in ${veh}`); // Show player chat message that car was spawned.

    // Handle what should happend when user enter something to chat.
    }else {
        // Spawn vehicle that user enter inside chat, on current player position.
        let veh = mp.vehicles.new(mp.joaat(model), new mp.Vector3(player.position.x, player.position.y, player.position.z), 
        {
            numberPlate: "ADMIN", // This won't work, we need to do it outside of vehicle creation method. 
            color: [[0, 255, 0],[0, 255, 0]] // Set vehicle colors.
        });

        veh.numberPlate = "ADMIN" // Proper set vehicle number plate with custom value.
        player.putIntoVehicle(veh, 0); // Put player inside vehicle that we create on driver seat.
        player.outputChatBox(`${player.name} put into ${model}`); // Output to chat that vehicle was spawn successfully.
    }
})

// Set player hp, with value from chat, or full if nothing was entered.
mp.events.addCommand("hp", (player, _, hp) => {
    if (hp === undefined) {
        player.health = 100
    }else {
        player.health = parseInt(hp);
        player.outputChatBox(`Player hp was ${player.health} and was set to ${hp}`)
    }
})

// Kill player command
mp.events.addCommand("kill", (player) => {
    player.health = 0;
})

// Teleport playe to specific location
mp.events.addCommand("tp", (player, _, x, y, z) => {
    if (parseFloat(x) === undefined || parseFloat(y) === undefined || parseFloat(z) === undefined) {
        player.outputChatBox("usage: /tp [x], [y], [z]")
    }else {
        player.outputChatBox("Player position changed").
        player.position = new mp.Vector3(parseFloat(x), parseFloat(y), parseFloat(z))
    }
})

// Command to set weather, which calls external event.
mp.events.addCommand("setweather", (player, _, weather) => {
    mp.events.call("setWeather", weather);
})

// Change player ped model
mp.events.addCommand("pedchange", (player, _, model) => {
    if (model !== undefined) {
        player.model = model;
    }else {
        player.outputChatBox(`usage: /pedchange model`);
    }
})

// Call clientside event to create camera
mp.events.addCommand("camera", (player) => {
    player.call("createCamera");
})

// Call clientside event to remove camera
mp.events.addCommand("camremove", (player) => {
    player.call("removeCam");
})