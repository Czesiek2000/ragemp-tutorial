/* 
    * This is your module root file.
*/

const skins = require('./skins');
const vehicles = require('./vehicles');
const weapons = require('./weapons.js');

// Or import like this: 
const { skins, vehicles, weapons } = require('./all.js');