const skins = [
    'mp_m_freeemode_01',
    'player_zero',
    'player_one',
    'player_two',
    // And lots more
]

const vehicles = [
    'adder',
    'turismor',
    't20',
    'cheburek',
    // And lots more
]

const weapons = [
    'weapon_grenade',
    'weapon_grenadelauncher',
    'weapon_carbinerifle',
    // And lots more
]

// Exports 
module.export.skins = skins;
module.export.vehicles = vehicles;
module.export.weapons = weapons;

// Or export like this:
module.exports = {
    skins,
    vehicles,
    weapons,
}