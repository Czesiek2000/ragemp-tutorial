/* 
    * This is main index.js client side file. Here we handle browser creation and destruction
*/
let browser;

// Add command to chat to open CEF
mp.events.add("playerCommand", (command) => {
    const args = command.split(/[ ]+/)
    const commandName = args[0]
    args.shift();
    
    // Replace value in "" with your command name
    if (commandName === "example") {
    
        // Create browser 
        browser = mp.browsers.new("package://cef/ui/index.html");
    
        // Display coursor
        setTimeout(() => {
            mp.gui.cursor.show(true, true);
        }, 500);
    
        // Show notification (optional)
        mp.game.graphics.notify("Example notification")

        
        // Command to destroy CEF
    }else if (commandName === "closeexample") {
        
        // Destroy CEF
        browser.destroy();

        // Hide cursor
        setTimeout(() => {
            mp.gui.cursor.show(false, false);
        }, 500);
    }
})

// Catch CEF calling
mp.events.add('fromBrowser', (param) => {
    // To check if we have connection CEF - client
    mp.game.graphics.notify(`Cef call with text: ${param}`)

    // Call CEF from client
    browser.execute(`showValue()`); // With parameters
    browser.execute(`showValue(\`CEF call test\`)`); // Without parameters
})