// Get fields from HTML and store in variable
const btn = document.getElementById('btn');
const btn2 = document.getElementById('btn2');
const output = document.getElementById('output');
const field = document.querySelector('.field');

// Listen for click event
btn.addEventListener('click', () => {
    // This will display text in browser
    output.innerHTML = "Text display after click button in browser"
    
});

btn2.addEventListener('click', () => {
    // Get value of field
    const fieldValue = field.value; 
    // Check if value is not empty
    if (fieldValue === "") {
        // If empty output message to screen
        output.innerHTML = "Input value cannot be empty"
    }else {
        // If not send data to client
        console.log(fieldValue);
        // This will send data to from CEF to client
         mp.trigger('fromBrowser', fieldValue);
    }
})

function showValue(param1="") {
    let display = `Value from server: ${param1}`
    output.innerHTML = display;
}