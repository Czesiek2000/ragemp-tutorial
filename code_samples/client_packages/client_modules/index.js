// This is your main index.js resource file. It should be placed under path/to/RAGEMP/server-files/client_packages/<client_modules(for this example, if you name your resource diffrently change this value to name of your client resource)>. 
// Just copy and paste this file and change something only if its not working properly.
const skins = require('./gamemode/data/skins');
const vehicles = require('./gamemode/data/vehicles');
const weapons = require('./gamemode/data/weapons');

mp.gui.chat.push(`We have ${skins[0]}, ${vehicles[0]}, ${weapons[0]}`)