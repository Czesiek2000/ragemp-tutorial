const NativeUI = require("./menu/nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const UIMenuListItem = NativeUI.UIMenuListItem;
const UIMenuCheckboxItem = NativeUI.UIMenuCheckboxItem;
const UIMenuSliderItem = NativeUI.UIMenuSliderItem;
const BadgeStyle = NativeUI.BadgeStyle;
const Point = NativeUI.Point;
const ItemsCollection = NativeUI.ItemsCollection;
const Color = NativeUI.Color;
const ListItem = NativeUI.ListItem;

const ui = new Menu("Test UI", "Test UI Subtitle", new Point(50, 50));
ui.Close();
ui.AddItem(new UIMenuItem("Spawn car", "Spawn turismor"));
locked = false;
ui.AddItem(new UIMenuCheckboxItem(
	"Lock",
	locked,
	"Lock vehicle that was spawned"
));

// ui.AddItem(new UIMenuItem("Lock", "Lock vehicle that was spawned"));

let item1 = new UIMenuListItem(
	"Weapons",
	"Give player specific weapons",
	new ItemsCollection(["weapon_grenade", "weapon_assaultsmg", "weapon_advancedrifle"])
);

ui.AddItem(item1);

ui.AddItem(new UIMenuItem("Heal", "Heal player"));

ui.ItemSelect.on((item) => {
	if (item.Text === "Spawn car") {
		mp.events.callRemote('spawnCar');
		mp.events.callRemote('log', 'Spawning car')
	}else if (item.Text === "Lock") {
		mp.events.callRemote('lockVehicle');
		locked = !locked;
	}else if (item.Text === "Heal") {
		mp.events.callRemote('healPlayer');
	}
})

ui.ListChange.on((item, index) => {
	switch (item) {
		case item1:
			mp.events.callRemote('playerWeapon', item.SelectedItem.DisplayText)
		break;
	}
})

ui.MenuClose.on(() => {
    mp.gui.chat.show(true);
    mp.gui.cursor.visible = false;
});


mp.keys.bind(0x71, false, () => { // F2
	if (ui.Visible) ui.Close();
	else ui.Open();
});