/* 
    * This is your main index.js file client side. 
    * This should be placed in PATH/TO/RAGEMP/server-files/client_packages.
    * Structure of client_packages folder simulate how you server client side file structure should look like.
    * require function can have only name of the whole module and it will look for main index.js file of this module
*/

require('client_modules');
require('test');
require('cef');
require('menu_sample');

// If you set up everything correctly you should see hello world in chat, and first values of each array.