<div align="center">
<h1> RAGEMP TUTORIAL </h1>
</div>
This repository is made to show new RAGEMP developers how read and use API to create custom scripts for RAGEMP. Also presenting them basics of Javascript, Nodejs, build in CEF

Link to live version can be found [here](https://czesiek2000.gitlab.io/ragemp-tutorial)

Inspired by tutorial made by Stuyk for AltV checkout his work [tutorial](https://altv.stuyk.com/), [github](https://github.com/Stuyk)

## Preview
![preview](./preview.png)

## Contributing
If you have some ideas what topics should be cover in this tutorial, feel free to reach me on ragemp forum [here](rage.mp/forums). 

If you are experience developer and have some ideas how to improve this tutorial, you can contribute to this project and create some more new content. 

More on that inside [contribution](https://czesiek2000.gitlab.io/ragemp-tutorial/guide/contribution.html)

### Development 
If you want to develop some more content to this tutorial, you should have nodejs installed on your computer and basic knowledge on Vuejs and Vuepress.

Install dependencies.
```bash
npm install
```
Run development server.
```bash
npm run dev
```
Build for production.
```bash
npm run build
```

If you :heart: or :thumbsup: this repository don't forget to leave :star: here on the repository or leave :speech_balloon: on the ragemp forum [here](rage.mp/forums). 
